package developer.arcfej.writemystory.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.IgnoreExtraProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import developer.arcfej.writemystory.model.database.DatabaseContract;

import static developer.arcfej.writemystory.model.database.DatabaseContract.DbKey;

@IgnoreExtraProperties
public class Chapter implements Parcelable {

    @Exclude
    public String chapterId;
    public String authorId;
    public String storyId;
    public String previousChapterId;
    public String nextChapterId;
    public String previousChoiceId;
    public Long depth;
    public Long longestChain;
    public List<String> choices;
    public String chapterBody;
    public String chapterBodyDraft;
    public String decisionText;
    public Long timestamp;
    public Boolean isPublished;

    /**
     * Empty constructor for RealtimeDatabase. Use the other constructor(s)
     */
    public Chapter() {

    }

    public Chapter(String authorId, String storyId, String previousChapterId,
                   String previousChoiceId, Boolean isPublished) {
        this.authorId = authorId;
        this.storyId = storyId;
        this.previousChapterId = previousChapterId;
        this.previousChoiceId = previousChoiceId;
        this.isPublished = isPublished;
    }


    public Map<String, Object> toMap() {
        HashMap<String, Object> values = new HashMap<>();
        values.put(DatabaseContract.DbKey.AUTHOR_ID.getKeyName(), authorId);
        values.put(DbKey.STORY_ID.getKeyName(), storyId);
        values.put(DbKey.PREVIOUS_CHAPTER_ID.getKeyName(), previousChapterId);
        values.put(DatabaseContract.DbKey.NEXT_CHAPTER_ID.getKeyName(), nextChapterId);
        values.put(DatabaseContract.DbKey.PREVIOUS_CHOICE_ID.getKeyName(), previousChoiceId);
        values.put(DbKey.DEPTH.getKeyName(), depth);
        values.put(DatabaseContract.DbKey.LONGEST_CHAIN.getKeyName(), longestChain);
        values.put(DbKey.CHOICES.getKeyName(), choices);
        values.put(DatabaseContract.DbKey.CHAPTER_BODY.getKeyName(), chapterBody);
        values.put(DbKey.CHAPTER_BODY_DRAFT.getKeyName(), chapterBodyDraft);
        values.put(DatabaseContract.DbKey.DECISION_TEXT.getKeyName(), decisionText);
        values.put(DatabaseContract.DbKey.TIMESTAMP.getKeyName(), timestamp);
        values.put(DbKey.IS_PUBLISHED.getKeyName(), isPublished);
        return values;
    }

    protected Chapter(Parcel in) {
        authorId = in.readString();
        storyId = in.readString();
        previousChapterId = in.readString();
        previousChoiceId = in.readString();
        nextChapterId = in.readString();
        depth = in.readByte() == 0x00 ? null : in.readLong();
        longestChain = in.readByte() == 0x00 ? null : in.readLong();
        if (in.readByte() == 0x01) {
            choices = new ArrayList<String>();
            in.readList(choices, String.class.getClassLoader());
        } else {
            choices = null;
        }
        chapterBody = in.readString();
        chapterBodyDraft = in.readString();
        decisionText = in.readString();
        timestamp = in.readByte() == 0x00 ? null : in.readLong();
        byte isPublishedVal = in.readByte();
        isPublished = isPublishedVal == 0x02 ? null : isPublishedVal != 0x00;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(authorId);
        dest.writeString(storyId);
        dest.writeString(previousChapterId);
        dest.writeString(nextChapterId);
        dest.writeString(previousChoiceId);
        if (depth == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(depth);
        }
        if (longestChain == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(longestChain);
        }
        if (choices == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(choices);
        }
        dest.writeString(chapterBody);
        dest.writeString(chapterBodyDraft);
        dest.writeString(decisionText);
        if (timestamp == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(timestamp);
        }
        if (isPublished == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (isPublished ? 0x01 : 0x00));
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Chapter> CREATOR = new Parcelable.Creator<Chapter>() {
        @Override
        public Chapter createFromParcel(Parcel in) {
            return new Chapter(in);
        }

        @Override
        public Chapter[] newArray(int size) {
            return new Chapter[size];
        }
    };
}