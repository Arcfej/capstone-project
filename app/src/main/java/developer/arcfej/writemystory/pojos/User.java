package developer.arcfej.writemystory.pojos;

import com.google.firebase.database.Exclude;
import com.google.firebase.firestore.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import developer.arcfej.writemystory.model.database.DatabaseContract;

@IgnoreExtraProperties
public class User {

    public String name;
    public String email;
    public boolean isAuthenticated;
    public List<String> stories;
    public List<String> chapters;

    public User() {

    }

    public User(String name, String email, boolean isAuthenticated) {
        this.name = name;
        this.email = email;
        this.isAuthenticated = isAuthenticated;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> values = new HashMap<>();
        values.put(DatabaseContract.DbKey.NAME.getKeyName(), name);
        values.put(DatabaseContract.DbKey.EMAIL.getKeyName(), email);
        values.put(DatabaseContract.DbKey.IS_AUTHENTICATED.getKeyName(), isAuthenticated);
        values.put(DatabaseContract.DbKey.STORIES.getKeyName(), stories);
        values.put(DatabaseContract.DbKey.CHAPTERS.getKeyName(), chapters);
        return values;
    }
}