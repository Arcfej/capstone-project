package developer.arcfej.writemystory.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.IgnoreExtraProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import developer.arcfej.writemystory.model.database.DatabaseContract;

import static developer.arcfej.writemystory.model.database.DatabaseContract.DbKey;

@IgnoreExtraProperties
public class Choice implements Parcelable {

    @Exclude
    public String choiceId;
    public String authorId;
    public String storyId;
    public String previousChapterId;
    public List<String> chapters;
    public String choiceText;
    public String decisionText;
    public Boolean isPublished;

    /**
     * Empty constructor for RealtimeDatabase. Use the other constructor(s)
     */
    public Choice() {

    }

    public Choice(String authorId, String storyId, String previousChapterId, Boolean isPublished) {
        this.authorId = authorId;
        this.storyId = storyId;
        this.previousChapterId = previousChapterId;
        this.isPublished = isPublished;
    }

    public Map<String, Object> toMap() {
        // Choice id excluded deliberately
        HashMap<String, Object> values = new HashMap<>();
        values.put(DatabaseContract.DbKey.AUTHOR_ID.getKeyName(), authorId);
        values.put(DbKey.STORY_ID.getKeyName(), storyId);
        values.put(DbKey.PREVIOUS_CHAPTER_ID.getKeyName(), previousChapterId);
        values.put(DbKey.CHAPTERS.getKeyName(), chapters);
        values.put(DbKey.CHOICE_TEXT.getKeyName(), choiceText);
        values.put(DbKey.DECISION_TEXT.getKeyName(), decisionText);
        values.put(DbKey.IS_PUBLISHED.getKeyName(), isPublished);
        return values;
    }

    protected Choice(Parcel in) {
        choiceId = in.readString();
        authorId = in.readString();
        storyId = in.readString();
        previousChapterId = in.readString();
        if (in.readByte() == 0x01) {
            chapters = new ArrayList<String>();
            in.readList(chapters, String.class.getClassLoader());
        } else {
            chapters = null;
        }
        choiceText = in.readString();
        decisionText = in.readString();
        byte isPublishedVal = in.readByte();
        isPublished = isPublishedVal == 0x02 ? null : isPublishedVal != 0x00;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(choiceId);
        dest.writeString(authorId);
        dest.writeString(storyId);
        dest.writeString(previousChapterId);
        if (chapters == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(chapters);
        }
        dest.writeString(choiceText);
        dest.writeString(decisionText);
        if (isPublished == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (isPublished ? 0x01 : 0x00));
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Choice> CREATOR = new Parcelable.Creator<Choice>() {
        @Override
        public Choice createFromParcel(Parcel in) {
            return new Choice(in);
        }

        @Override
        public Choice[] newArray(int size) {
            return new Choice[size];
        }
    };
}