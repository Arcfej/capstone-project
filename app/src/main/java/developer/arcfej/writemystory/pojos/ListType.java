package developer.arcfej.writemystory.pojos;

public enum ListType {
    STORIES,
    CHAPTERS,
    MY_STORIES,
    MY_CHAPTERS
}
