package developer.arcfej.writemystory.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.IgnoreExtraProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import developer.arcfej.writemystory.model.database.DatabaseContract;

import static developer.arcfej.writemystory.model.database.DatabaseContract.DbKey;

@IgnoreExtraProperties
public class Story implements Parcelable {

    @Exclude
    public String storyId;
    public String authorId;
    public String shortDescription;
    public String shortDescriptionDraft;
    public String firstChapterId;
    public List<String> contributors;
    public Long chaptersCount;
    public Long longestChain;
    public String storyTitle;
    public String storyTitleDraft;
    public Long timestamp;
    public Boolean isPublished;

    /**
     * Empty constructor for RealtimeDatabase. Use the other constructor(s)
     */
    public Story() {

    }

    public Story(String authorId, Boolean isPublished) {
        this.authorId = authorId;
        this.isPublished = isPublished;
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> values = new HashMap<>();
        values.put(DatabaseContract.DbKey.AUTHOR_ID.getKeyName(), authorId);
        values.put(DbKey.SHORT_DESCRIPTION.getKeyName(), shortDescription);
        values.put(DatabaseContract.DbKey.SHORT_DESCRIPTION_DRAFT.getKeyName(), shortDescriptionDraft);
        values.put(DbKey.FIRST_CHAPTER_ID.getKeyName(), firstChapterId);
        values.put(DbKey.CONTRIBUTORS.getKeyName(), contributors);
        values.put(DatabaseContract.DbKey.CHAPTERS_COUNT.getKeyName(), chaptersCount);
        values.put(DatabaseContract.DbKey.LONGEST_CHAIN.getKeyName(), longestChain);
        values.put(DatabaseContract.DbKey.STORY_TITLE.getKeyName(), storyTitle);
        values.put(DbKey.STORY_TITLE_DRAFT.getKeyName(), storyTitleDraft);
        values.put(DatabaseContract.DbKey.TIMESTAMP.getKeyName(), timestamp);
        values.put(DbKey.IS_PUBLISHED.getKeyName(), isPublished);
        return values;
    }

    protected Story(Parcel in) {
        authorId = in.readString();
        shortDescription = in.readString();
        shortDescriptionDraft = in.readString();
        firstChapterId = in.readString();
        if (in.readByte() == 0x01) {
            contributors = new ArrayList<>();
            in.readList(contributors, String.class.getClassLoader());
        } else {
            contributors = null;
        }
        chaptersCount = in.readByte() == 0x00 ? null : in.readLong();
        longestChain = in.readByte() == 0x00 ? null : in.readLong();
        storyTitle = in.readString();
        storyTitleDraft = in.readString();
        timestamp = in.readByte() == 0x00 ? null : in.readLong();
        byte isPublishedVal = in.readByte();
        isPublished = isPublishedVal == 0x02 ? null : isPublishedVal != 0x00;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(authorId);
        dest.writeString(shortDescription);
        dest.writeString(shortDescriptionDraft);
        dest.writeString(firstChapterId);
        if (contributors == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(contributors);
        }
        if (chaptersCount == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(chaptersCount);
        }
        if (longestChain == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(longestChain);
        }
        dest.writeString(storyTitle);
        dest.writeString(storyTitleDraft);
        if (timestamp == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(timestamp);
        }
        if (isPublished == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (isPublished ? 0x01 : 0x00));
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Story> CREATOR = new Parcelable.Creator<Story>() {
        @Override
        public Story createFromParcel(Parcel in) {
            return new Story(in);
        }

        @Override
        public Story[] newArray(int size) {
            return new Story[size];
        }
    };
}