package developer.arcfej.writemystory.views.reading;

import developer.arcfej.writemystory.views.BaseView;

public interface ReadingView extends BaseView {

    void showLoading();
}