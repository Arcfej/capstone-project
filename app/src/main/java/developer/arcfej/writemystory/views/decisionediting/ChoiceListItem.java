package developer.arcfej.writemystory.views.decisionediting;

public interface ChoiceListItem {

    String getChoiceText();

    void setChoiceText(String choiceText);

    void configureButtonClickListener(int buttonFunction, int position);

    void setInputFieldVisibility(int visibility);
}