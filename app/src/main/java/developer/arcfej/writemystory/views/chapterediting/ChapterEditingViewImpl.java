package developer.arcfej.writemystory.views.chapterediting;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import developer.arcfej.writemystory.EditingActivity;
import developer.arcfej.writemystory.R;
import developer.arcfej.writemystory.utilities.BottomActionsHelper;

public class ChapterEditingViewImpl implements ChapterEditingView {

    private final View rootView;
    @BindView(R.id.til_chapter_body_input_holder)
    TextInputLayout chapterBodyInput;
    @BindView(R.id.pb_loading)
    ProgressBar loading;
    @BindView(R.id.bnv_action_buttons)
    BottomNavigationView bottomActions;

    public ChapterEditingViewImpl(LayoutInflater inflater, ViewGroup container, Bundle state) {
        rootView = inflater.inflate(R.layout.fragment_new_chapter, container, false);
        ButterKnife.bind(this, rootView);

        if (state != null && state.containsKey(STATE_CHAPTER_BODY_INPUT)) {
            chapterBodyInput.getEditText().setText(state.getString(STATE_CHAPTER_BODY_INPUT));
        }

        BottomActionsHelper.removeShiftMode(bottomActions);
        bottomActions.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_discard:
                        EventBus.getDefault()
                                .post(new EditingActivity.DiscardActionEvent(false));
                        return true;
                    case R.id.action_save:
                        EventBus.getDefault().post(new SaveChapterEvent(
                                chapterBodyInput.getEditText().getText().toString().trim()
                        ));
                        return true;
                    case R.id.action_finish:
                        EventBus.getDefault().post(new FinishChapterEvent(
                                chapterBodyInput.getEditText().getText().toString().trim()
                        ));
                    case R.id.action_add_choices:
                        EventBus.getDefault().post(new AddChoicesEvent(
                                chapterBodyInput.getEditText().getText().toString().trim()));
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Bundle getViewState() {
        Bundle state = new Bundle();
        state.putString(
                STATE_CHAPTER_BODY_INPUT,
                chapterBodyInput.getEditText().getText().toString().trim()
        );
        return state;
    }

    @Override
    public void setInputField(String chapterBody) {
        if (chapterBody != null) {
            chapterBodyInput.getEditText().setText(chapterBody);
        }
    }

    @Override
    public void setErrorMessage(@Nullable String chapterBodyError) {
        if (TextUtils.isEmpty(chapterBodyError)) {
            chapterBodyInput.setErrorEnabled(false);
        } else {
            chapterBodyInput.setErrorEnabled(true);
            chapterBodyInput.setError(chapterBodyError);
        }
    }

    @Override
    public void showLoading() {
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loading.setVisibility(View.GONE);
    }

    public static class SaveChapterEvent {

        public final String chapterBody;

        private SaveChapterEvent(String chapterBody) {
            this.chapterBody = chapterBody;
        }
    }

    public static class FinishChapterEvent {

        public final String chapterBody;

        public FinishChapterEvent(String chapterBody) {
            this.chapterBody = chapterBody;
        }
    }

    public static class AddChoicesEvent {

        public final String chapterBody;

        public AddChoicesEvent(String chapterBody) {
            this.chapterBody = chapterBody;
        }
    }
}