package developer.arcfej.writemystory.views.chapterediting;

import android.support.annotation.Nullable;

import developer.arcfej.writemystory.views.BaseView;

public interface ChapterEditingView extends BaseView {

    String STATE_CHAPTER_BODY_INPUT = "chapter_body";

    void setInputField(String chapterBody);

    void setErrorMessage(@Nullable String chapterBodyError);

    void showLoading();

    void hideLoading();
}