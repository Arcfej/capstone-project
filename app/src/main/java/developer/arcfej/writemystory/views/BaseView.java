package developer.arcfej.writemystory.views;

import android.os.Bundle;
import android.view.View;

public interface BaseView {

    View getRootView();

    Bundle getViewState();
}