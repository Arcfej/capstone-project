package developer.arcfej.writemystory.views.decisionediting;

import developer.arcfej.writemystory.views.BaseView;

public interface DecisionEditingView extends BaseView {

    String STATE_DECISION_TEXT = "decision_text";
    String STATE_CHOICE_TEXT_LIST = "choice_text_list";

    void setDecisionText(String decisionText);

    void showLoading();

    void hideLoading();
}