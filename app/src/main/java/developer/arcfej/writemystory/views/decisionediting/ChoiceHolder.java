package developer.arcfej.writemystory.views.decisionediting;

import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import developer.arcfej.writemystory.R;

public class ChoiceHolder extends RecyclerView.ViewHolder implements ChoiceListItem {

    public static final int BUTTON_FUNCTION_ADD = 0;
    public static final int BUTTON_FUNCTION_REMOVE = 1;

    @BindView(R.id.til_choice_input_holder)
    TextInputLayout choiceInput;
    @BindView(R.id.ib_add_remove_choice)
    ImageButton addRemoveButton;

    private ChoiceHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public static ChoiceHolder constructWithInflating(ViewGroup parent) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_choice, parent, false);
        return new ChoiceHolder(itemView);
    }

    @Override
    public String getChoiceText() {
        return choiceInput.getEditText().getText().toString().trim();
    }

    @Override
    public void setChoiceText(String choiceText) {
        choiceInput.getEditText().setText(choiceText);
    }

    @Override
    public void configureButtonClickListener(int buttonFunction, final int choicePosition) {
        // TODO change icon
        View.OnClickListener clickListener;
        switch (buttonFunction) {
            case BUTTON_FUNCTION_ADD:
                clickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EventBus.getDefault().post(new AddNewChoiceEvent());
                    }
                };
                break;
            case BUTTON_FUNCTION_REMOVE:
                clickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EventBus.getDefault().post(new RemoveChoiceEvent(choicePosition));
                    }
                };
                break;
            default:
                clickListener = null;
        }
        addRemoveButton.setOnClickListener(clickListener);
    }

    @Override
    public void setInputFieldVisibility(int visibility) {
        switch (visibility) {
            case View.VISIBLE:
            case View.INVISIBLE:
            case View.GONE:
                choiceInput.setVisibility(visibility);
        }
    }

    public static class AddNewChoiceEvent {

    }

    public static class RemoveChoiceEvent {

        public final int choicePosition;

        public RemoveChoiceEvent(int choicePosition) {
            this.choicePosition = choicePosition;
        }
    }
}
