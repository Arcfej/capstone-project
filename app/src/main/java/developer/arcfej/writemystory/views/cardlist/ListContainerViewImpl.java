package developer.arcfej.writemystory.views.cardlist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import developer.arcfej.writemystory.R;
import developer.arcfej.writemystory.presenters.BasePresenter;

public class ListContainerViewImpl implements ListContainerView {

    private final View rootView;
    @BindView(R.id.upper_header)
    View upperHeader;
    TextView upperTitle;
    TextView upperSubtitle;
    @BindView(R.id.fl_upper_fragment_holder)
    FrameLayout upperFragment;
    @BindView(R.id.lower_header)
    View lowerHeader;
    TextView lowerTitle;
    TextView lowerSubtitle;
    @BindView(R.id.fl_lower_fragment_holder)
    FrameLayout lowerFragment;
    @BindView(R.id.fab_add_new_story)
    FloatingActionButton fabAddNewStory;

    public ListContainerViewImpl(LayoutInflater inflater, ViewGroup parent, BasePresenter presenter,
                                 boolean isEditingAllowed) {
        rootView = inflater.inflate(R.layout.activity_listing, parent, false);
        ButterKnife.bind(this, rootView);
        upperTitle = upperHeader.findViewById(R.id.tv_title);
        upperSubtitle = upperHeader.findViewById(R.id.tv_subtitle);
        lowerTitle = lowerHeader.findViewById(R.id.tv_title);
        lowerSubtitle = lowerHeader.findViewById(R.id.tv_subtitle);
        if (isEditingAllowed) {
            fabAddNewStory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new FabClickEvent());
                }
            });
        } else {
            fabAddNewStory.setVisibility(View.GONE);
        }
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }

    @Override
    public void setUpperFragmentVisibility(int visibility) {
        if (visibility == View.VISIBLE || visibility == View.INVISIBLE || visibility == View.GONE) {
            upperFragment.setVisibility(visibility);
        }
    }

    @Override
    public void setLowerFragmentVisibility(int visibility) {
        if (visibility == View.VISIBLE || visibility == View.INVISIBLE || visibility == View.GONE) {
            lowerFragment.setVisibility(visibility);
        }
    }

    @Override
    public void setUpperHeader(@Nullable String title, @Nullable String subtitle, boolean clickable) {
        upperHeader.setVisibility((TextUtils.isEmpty(title) && TextUtils.isEmpty(subtitle)) ?
                View.GONE : View.VISIBLE);
        upperTitle.setVisibility(TextUtils.isEmpty(title) ? View.GONE : View.VISIBLE);
        upperTitle.setText(title);
        upperSubtitle.setVisibility(TextUtils.isEmpty(subtitle) ? View.GONE : View.VISIBLE);
        upperSubtitle.setText(subtitle);
        upperHeader.setOnClickListener(clickable ?
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EventBus.getDefault().post(new UpperHeaderClickEvent());
                    }
                } :
                null
        );
    }

    @Override
    public void setLowerHeader(@Nullable String title, @Nullable String subtitle, boolean clickable) {
        lowerHeader.setVisibility((TextUtils.isEmpty(title) && TextUtils.isEmpty(subtitle)) ?
                View.GONE : View.VISIBLE);
        lowerTitle.setVisibility(TextUtils.isEmpty(title) ? View.GONE : View.VISIBLE);
        lowerTitle.setText(title);
        lowerSubtitle.setVisibility(TextUtils.isEmpty(subtitle) ? View.GONE : View.VISIBLE);
        lowerSubtitle.setText(subtitle);
        lowerHeader.setOnClickListener(clickable ?
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EventBus.getDefault().post(new LowerHeaderClickEvent());
                    }
                } :
                null
        );
    }

    public static class UpperHeaderClickEvent {

    }

    public static class LowerHeaderClickEvent {

    }

    public static class FabClickEvent {

    }
}