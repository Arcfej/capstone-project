package developer.arcfej.writemystory.views.storyediting;

import android.support.annotation.Nullable;

import developer.arcfej.writemystory.views.BaseView;

public interface StoryEditingView extends BaseView {

    String STATE_TITLE_INPUT = "title";
    String STATE_SHORT_DESCRIPTION_INPUT = "short_description";

    void setInputFields(String title, String shortDescription);

    void setErrorMessages(@Nullable String titleError, @Nullable String shortDescriptionError);

    void showLoading();

    void hideLoading();
}