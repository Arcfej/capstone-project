package developer.arcfej.writemystory.views.reading;

import developer.arcfej.writemystory.views.BaseView;

public interface StoryInfoView extends BaseView {

    void showLoading();
}