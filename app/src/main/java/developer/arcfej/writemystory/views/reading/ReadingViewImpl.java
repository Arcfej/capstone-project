package developer.arcfej.writemystory.views.reading;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import developer.arcfej.writemystory.R;

public class ReadingViewImpl implements ReadingView {

    private static final String STATE_SCROLL_POSITION = "scroll_position";

    private final View rootView;
    @BindView(R.id.button_previous_chapter)
    Button previousChapterButton;
    @BindView(R.id.tv_story_reader)
    TextView reader;
    @BindView(R.id.button_next_chapter)
    Button nextChapterButton;
    @BindView(R.id.pb_loading)
    ProgressBar loading;

    public ReadingViewImpl(LayoutInflater inflater, ViewGroup container, String text,
                           boolean hasPreviousChapter, boolean hasNextChapter, Bundle state) {
        rootView = inflater.inflate(R.layout.fragment_reading, container, false);
        ButterKnife.bind(this, rootView);

        reader.setText(text);
        previousChapterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new PreviousChapterButtonClickEvent());
            }
        });
        nextChapterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new NextChapterButtonClickEvent());
            }
        });
        nextChapterButton.setEnabled(hasNextChapter);
        if (!hasPreviousChapter) previousChapterButton.setVisibility(View.GONE);

        if (state != null && state.containsKey(STATE_SCROLL_POSITION)) {
            this.reader.scrollTo(0, state.getInt(STATE_SCROLL_POSITION));
        }
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Bundle getViewState() {
        Bundle state = new Bundle();
        state.putInt(STATE_SCROLL_POSITION, reader.getScrollY());
        return state;
    }

    @Override
    public void showLoading() {
        loading.setVisibility(View.VISIBLE);
    }

    public static class PreviousChapterButtonClickEvent {

    }

    public static class NextChapterButtonClickEvent {

    }
}