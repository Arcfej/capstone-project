package developer.arcfej.writemystory.views.storyediting;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomNavigationView.OnNavigationItemSelectedListener;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import developer.arcfej.writemystory.EditingActivity;
import developer.arcfej.writemystory.R;
import developer.arcfej.writemystory.utilities.BottomActionsHelper;

public class StoryEditingViewImpl implements StoryEditingView {

    private final View rootView;
    @BindView(R.id.til_story_title_input_holder)
    TextInputLayout titleInput;
    @BindView(R.id.til_story_short_description_input_holder)
    TextInputLayout shortDescriptionInput;
    @BindView(R.id.pb_loading)
    ProgressBar loading;
    @BindView(R.id.bnv_action_buttons)
    BottomNavigationView bottomActions;

    public StoryEditingViewImpl(LayoutInflater inflater, final ViewGroup container, Bundle state) {
        rootView = inflater.inflate(R.layout.fragment_new_story, container, false);
        ButterKnife.bind(this, rootView);

        if (state != null) {
            if (state.containsKey(STATE_TITLE_INPUT)) {
                titleInput.getEditText().setText(state.getString(STATE_TITLE_INPUT));
            }
            if (state.containsKey(STATE_SHORT_DESCRIPTION_INPUT)) {
                shortDescriptionInput.getEditText()
                        .setText(state.getString(STATE_SHORT_DESCRIPTION_INPUT));
            }
        }

        BottomActionsHelper.removeShiftMode(bottomActions);
        bottomActions.setOnNavigationItemSelectedListener(new OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_discard:
                        EventBus.getDefault()
                                .post(new EditingActivity.DiscardActionEvent(false));
                        return true;
                    case R.id.action_save:
                        EventBus.getDefault().post(new SaveStoryEvent(
                                titleInput.getEditText().getText().toString().trim(),
                                shortDescriptionInput.getEditText().getText().toString().trim()
                        ));
                        return true;
                    case R.id.action_continue:
                        EventBus.getDefault().post(new ContinueStoryEvent(
                                titleInput.getEditText().getText().toString().trim(),
                                shortDescriptionInput.getEditText().getText().toString().trim()
                        ));
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Bundle getViewState() {
        Bundle state = new Bundle();
        state.putString(STATE_TITLE_INPUT, titleInput.getEditText().getText().toString().trim());
        state.putString(
                STATE_SHORT_DESCRIPTION_INPUT,
                shortDescriptionInput.getEditText().getText().toString().trim()
        );
        return state;
    }

    @Override
    public void setInputFields(String title, String shortDescription) {
        if (title != null) {
            titleInput.getEditText().setText(title);
        }
        if (shortDescription != null) {
            shortDescriptionInput.getEditText().setText(shortDescription);
        }
    }

    @Override
    public void setErrorMessages(@Nullable String titleError, @Nullable String shortDescriptionError) {
        if (TextUtils.isEmpty(titleError)) {
            titleInput.setErrorEnabled(false);
        } else {
            titleInput.setErrorEnabled(true);
            titleInput.setError(titleError);
        }
        if (TextUtils.isEmpty(shortDescriptionError)) {
            shortDescriptionInput.setErrorEnabled(false);
        } else {
            shortDescriptionInput.setErrorEnabled(true);
            shortDescriptionInput.setError(shortDescriptionError);
        }
    }

    @Override
    public void showLoading() {
            loading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
            loading.setVisibility(View.GONE);
    }

    public static class SaveStoryEvent {

        public final String title;
        public final String shortDescription;

        public SaveStoryEvent(String title, String shortDescription) {
            this.title = title;
            this.shortDescription = shortDescription;
        }
    }

    public static class ContinueStoryEvent {

        public final String title;
        public final String shortDescription;

        public ContinueStoryEvent(String title, String shortDescription) {
            this.title = title;
            this.shortDescription = shortDescription;
        }
    }
}