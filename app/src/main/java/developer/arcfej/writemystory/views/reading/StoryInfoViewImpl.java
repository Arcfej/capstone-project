package developer.arcfej.writemystory.views.reading;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import developer.arcfej.writemystory.R;

public class StoryInfoViewImpl implements StoryInfoView {

    private static final String STATE_SCROLL_POSITION = "scroll_position";

    private final View rootView;
    @BindView(R.id.tv_story_title)
    TextView title;
    @BindView(R.id.tv_story_author)
    TextView author;
    @BindView(R.id.tv_story_short_description)
    TextView shortDescription;
    @BindView(R.id.button_first_chapter)
    Button firstChapterButton;
    @BindView(R.id.pb_loading)
    ProgressBar loading;

    public StoryInfoViewImpl(LayoutInflater inflater, ViewGroup container, String title, String author,
                             String shortDescription, boolean hasFirstChapter, Bundle state) {
        rootView = inflater.inflate(R.layout.fragment_story_info, container, false);
        ButterKnife.bind(this, rootView);

        this.title.setText(title);
        this.author.setText(rootView.getResources().getString(R.string.story_card_author, author));
        this.shortDescription.setText(shortDescription);
        firstChapterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new FirstChapterButtonClickEvent());
            }
        });
        firstChapterButton.setEnabled(hasFirstChapter);

        if (state != null && state.containsKey(STATE_SCROLL_POSITION)) {
            this.shortDescription.scrollTo(0, state.getInt(STATE_SCROLL_POSITION));
        }
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Bundle getViewState() {
        Bundle state = new Bundle();
        state.putInt(STATE_SCROLL_POSITION, shortDescription.getScrollY());
        return state;
    }

    @Override
    public void showLoading() {
        loading.setVisibility(View.VISIBLE);
    }

    public static class FirstChapterButtonClickEvent {

    }
}