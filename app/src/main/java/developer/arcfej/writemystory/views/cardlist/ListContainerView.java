package developer.arcfej.writemystory.views.cardlist;

import android.support.annotation.Nullable;

import developer.arcfej.writemystory.views.BaseView;

public interface ListContainerView extends BaseView {

    void setUpperFragmentVisibility(int visibility);

    void setLowerFragmentVisibility(int visibility);

    void setUpperHeader(@Nullable String title, @Nullable String subtitle, boolean clickable);

    void setLowerHeader(@Nullable String title, @Nullable String subtitle, boolean clickable);
}