package developer.arcfej.writemystory.views.cardlist;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import developer.arcfej.writemystory.R;

public class CardListViewImpl implements CardListView {

    private final View rootView;

    @BindView(R.id.rv_list)
    RecyclerView list;

    public CardListViewImpl(LayoutInflater inflater, ViewGroup container,
                            RecyclerView.Adapter presenter) {
        rootView = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, rootView);

        list.setAdapter(presenter);
        list.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }
}