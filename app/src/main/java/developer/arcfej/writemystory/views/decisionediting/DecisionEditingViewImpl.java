package developer.arcfej.writemystory.views.decisionediting;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import developer.arcfej.writemystory.EditingActivity;
import developer.arcfej.writemystory.R;
import developer.arcfej.writemystory.utilities.BottomActionsHelper;

public class DecisionEditingViewImpl implements DecisionEditingView {

    private final View rootView;
    @BindView(R.id.til_decision_input_holder)
    TextInputLayout decisionTextInput;
    @BindView(R.id.rv_choice_list)
    RecyclerView choiceList;
    @BindView(R.id.pb_loading)
    ProgressBar loading;
    @BindView(R.id.bnv_action_buttons)
    BottomNavigationView bottomActions;

    private RecyclerView.LayoutManager layoutManager;

    public DecisionEditingViewImpl(LayoutInflater inflater, ViewGroup container,
                                   RecyclerView.Adapter presenter) {
        rootView = inflater.inflate(R.layout.fragment_new_decision, container, false);
        ButterKnife.bind(this, rootView);

        choiceList.setAdapter(presenter);
        layoutManager = new LinearLayoutManager(rootView.getContext());
        choiceList.setLayoutManager(layoutManager);

        BottomActionsHelper.removeShiftMode(bottomActions);
        bottomActions.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_discard:
                        EventBus.getDefault()
                                .post(new EditingActivity.DiscardActionEvent(false));
                        return true;
                    case R.id.action_save:
                        EventBus.getDefault().post(new SaveDecisionEvent(
                                decisionTextInput.getEditText().getText().toString().trim()
                        ));
                        return true;
                    case R.id.action_publish:
                        EventBus.getDefault().post(new PublishChapterEvent(
                                decisionTextInput.getEditText().getText().toString().trim()
                        ));
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Bundle getViewState() {
        Bundle state = new Bundle();
        state.putString(
                STATE_DECISION_TEXT, decisionTextInput.getEditText().getText().toString().trim()
        );
        return state;
    }

    @Override
    public void setDecisionText(String decisionText) {
        if (decisionText != null) {
            decisionTextInput.getEditText().setText(decisionText);
        }
    }

    @Override
    public void showLoading() {
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loading.setVisibility(View.GONE);
    }

    private static class SaveDecisionEvent {

        public final String decisionText;

        private SaveDecisionEvent(String decisionText) {
            this.decisionText = decisionText;
        }
    }

    private static class PublishChapterEvent {

        public final String decisionText;

        private PublishChapterEvent(String decisionText) {
            this.decisionText = decisionText;
        }
    }
}
