package developer.arcfej.writemystory.views.cardlist;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import developer.arcfej.writemystory.R;

public class StoryHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tv_card_title)
    TextView titleView;
    @BindView(R.id.tv_card_subtitle)
    TextView subtitleView;
    @BindView(R.id.tv_card_body)
    TextView bodyView;
    @BindView(R.id.ll_action_container)
    ViewGroup actionContainer;



    private StoryHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    private static StoryHolder constructWithInflating(ViewGroup parent) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.story_card, parent, false);
        return new StoryHolder(itemView);
    }

    public static class Builder {

        private StoryHolder storyHolder;

        public Builder(StoryHolder storyCard, boolean removeActions) {
            this.storyHolder = storyCard;
            if (removeActions) {
                storyHolder.actionContainer.removeAllViews();
            }
        }

        public Builder(ViewGroup parent) {
            this(constructWithInflating(parent), false);
        }

        public Builder addTitle(String title) {
            if (!TextUtils.isEmpty(title)) {
                storyHolder.titleView.setText(title);
                storyHolder.titleView.setVisibility(View.VISIBLE);
            } else {
                storyHolder.titleView.setText("");
                storyHolder.titleView.setVisibility(View.GONE);
            }
            return this;
        }

        public Builder setSubtitle(String subtitle) {
            if (!TextUtils.isEmpty(subtitle)) {
                storyHolder.subtitleView.setText(subtitle);
                storyHolder.subtitleView.setVisibility(View.VISIBLE);
            } else {
                storyHolder.subtitleView.setText("");
                storyHolder.subtitleView.setVisibility(View.GONE);
            }
            return this;
        }

        public Builder setBody(String body) {
            if (!TextUtils.isEmpty(body)) {
                storyHolder.bodyView.setText(body);
                storyHolder.bodyView.setVisibility(View.VISIBLE);
            } else {
                storyHolder.bodyView.setText("");
                storyHolder.bodyView.setVisibility(View.GONE);
            }
            return this;
        }

        public Builder addAction(String action, View.OnClickListener actionClickListener) {
            View actionButton = LayoutInflater.from(storyHolder.actionContainer.getContext())
                    .inflate(R.layout.action_button, storyHolder.actionContainer, false);
            storyHolder.actionContainer.addView(actionButton);
            ((TextView) actionButton).setText(action);
            actionButton.setOnClickListener(actionClickListener);
            return this;
        }

        public Builder addCardClickListener(View.OnClickListener onCardClickListener) {
            storyHolder.itemView.setOnClickListener(onCardClickListener);
            return this;
        }

        public StoryHolder build() {
            return storyHolder;
        }
    }
}