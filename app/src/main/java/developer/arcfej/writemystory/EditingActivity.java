package developer.arcfej.writemystory;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import developer.arcfej.writemystory.fragments.ChapterEditingFragment;
import developer.arcfej.writemystory.fragments.DecisionEditingFragment;
import developer.arcfej.writemystory.fragments.ReadingFragment;
import developer.arcfej.writemystory.fragments.StoryEditingFragment;
import developer.arcfej.writemystory.model.AppDataManager;
import developer.arcfej.writemystory.model.DataManager;
import developer.arcfej.writemystory.pojos.Chapter;

public class EditingActivity extends AppCompatActivity {

    public static final String ACTION_NEW_STORY = "new_story";
    public static final String ACTION_NEW_CHAPTER = "new_chapter";
    public static final String ACTION_NEW_DECISION = "new_decision";

    public static final String INTENT_EXTRA_STORY_ID = "story_id";
    public static final String INTENT_EXTRA_PREVIOUS_CHAPTER_ID = "previous_chapter_id";
    public static final String INTENT_EXTRA_PREVIOUS_CHOICE_ID = "previous_choice_id";

    private static final String TAG_FRAGMENT_READ = "read";
    private static final String TAG_FRAGMENT_STORY_EDITING = "story_editing";
    private static final String TAG_FRAGMENT_CHAPTER_EDITING = "chapter_editing";
    private static final String TAG_FRAGMENT_DECISION_EDITING = "decision_editing";

    private FirebaseAnalytics firebaseAnalytics;
    private DataManager dataManager;

    private Fragment readFragment;
    private Fragment storyEditingFragment;
    private Fragment chapterEditingFragment;
    private Fragment decisionEditingFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editing);

        // Initialize Firebase Analytics
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        dataManager = AppDataManager.getInstance(firebaseAnalytics);

        readFragment = getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT_READ);
        if (readFragment == null) {
            readFragment = new ReadingFragment();
        }
        storyEditingFragment = getSupportFragmentManager()
                .findFragmentByTag(TAG_FRAGMENT_STORY_EDITING);
        if (storyEditingFragment == null) {
            storyEditingFragment = new StoryEditingFragment();
        }
        chapterEditingFragment = getSupportFragmentManager()
                .findFragmentByTag(TAG_FRAGMENT_CHAPTER_EDITING);
        if (chapterEditingFragment == null) {
            chapterEditingFragment = new ChapterEditingFragment();
        }
        decisionEditingFragment = getSupportFragmentManager()
                .findFragmentByTag(TAG_FRAGMENT_DECISION_EDITING);
        if (decisionEditingFragment == null) {
            decisionEditingFragment = new DecisionEditingFragment();
        }

        if (savedInstanceState == null) {
            evaluateIntent(getIntent());
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        evaluateIntent(intent);

    }

    private void evaluateIntent(Intent intent) {
        String action = intent.getAction();
        if (action == null) return;
        switch (action) {
            case ACTION_NEW_STORY:
                setTitle(getString(R.string.app_bar_title_new_story));
                storyEditingFragment = new StoryEditingFragment();
                showStoryEditingFragment(null);
                break;
            case ACTION_NEW_CHAPTER:
                setTitle(getString(R.string.app_bar_title_new_chapter));
                Bundle args = new Bundle();
                args.putString(
                        ChapterEditingFragment.ARGUMENT_STORY_ID,
                        intent.getStringExtra(INTENT_EXTRA_STORY_ID)
                );
                args.putString(
                        ChapterEditingFragment.ARGUMENT_PREVIOUS_CHAPTER_ID,
                        intent.getStringExtra(INTENT_EXTRA_PREVIOUS_CHAPTER_ID)
                );
                chapterEditingFragment = new ChapterEditingFragment();
                showChapterEditingFragment(args);
                break;
        }
    }

    private void showStoryEditingFragment(@Nullable Bundle arguments) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (storyEditingFragment.isAdded()) {
            transaction.show(storyEditingFragment);
            // TODO is setting arguments necessary here? Than I should create a new method for it
        } else {
            storyEditingFragment.setArguments(arguments);
            transaction.addToBackStack(TAG_FRAGMENT_STORY_EDITING)
                    .replace(
                            R.id.fl_fragment_holder,
                            storyEditingFragment,
                            TAG_FRAGMENT_STORY_EDITING
                    );
        }
        transaction.commit();
    }

    private void showChapterEditingFragment(@Nullable Bundle arguments) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (chapterEditingFragment.isAdded()) {
            transaction.show(chapterEditingFragment);
            // TODO is setting arguments necessary here? Than I should create a new method for it
        } else {
            chapterEditingFragment.setArguments(arguments);
            transaction.addToBackStack(TAG_FRAGMENT_CHAPTER_EDITING)
                    .replace(
                            R.id.fl_fragment_holder,
                            chapterEditingFragment,
                            TAG_FRAGMENT_CHAPTER_EDITING
                    );
        }
        transaction.commit();
    }

    private void showDecisionEditingFragment(@Nullable Bundle arguments) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (decisionEditingFragment.isAdded()) {
            transaction.show(decisionEditingFragment);
            // TODO is setting arguments necessary here? Than I should create a new method for it
        } else {
            decisionEditingFragment.setArguments(arguments);
            transaction.addToBackStack(TAG_FRAGMENT_DECISION_EDITING)
                    .replace(
                            R.id.fl_fragment_holder,
                            decisionEditingFragment,
                            TAG_FRAGMENT_DECISION_EDITING
                    );
        }
        transaction.commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onBackPressed() {
        if (storyEditingFragment != null && storyEditingFragment.isVisible()) {
            showDiscardConfirmDialog();
        } else if (chapterEditingFragment != null && chapterEditingFragment.isVisible()) {
            showDiscardConfirmDialog();
        } else if (decisionEditingFragment != null && decisionEditingFragment.isVisible()) {
            showDiscardConfirmDialog();
        } else {
            super.onBackPressed();
        }
    }

    @Subscribe
    public void onDiscardAction(DiscardActionEvent event) {
        if (!event.isDiscardDecided) {
            showDiscardConfirmDialog();
        }
    }

    private void showDiscardConfirmDialog() {
        new AlertDialog.Builder(this)
                .setTitle(this.getText(R.string.discard_confirm_title))
                .setMessage(this.getText(R.string.discard_confirm_message))
                .setPositiveButton(
                        this.getText(R.string.discard_confirm_yes),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                EventBus.getDefault()
                                        .post(new DiscardActionEvent(true));
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton(
                        this.getText(R.string.discard_confirm_no),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                .show();
    }

    @Subscribe
    public void onNewChapterStarted(StartNewChapterEvent event) {
        Intent newIntent = new Intent(this, this.getClass());
        newIntent.putExtra(INTENT_EXTRA_STORY_ID, event.parentStoryId);
        newIntent.putExtra(INTENT_EXTRA_PREVIOUS_CHAPTER_ID, event.previousChapterId);
        newIntent.putExtra(INTENT_EXTRA_PREVIOUS_CHOICE_ID, event.previousChoiceId);
        newIntent.setAction(ACTION_NEW_CHAPTER);

        setIntent(newIntent);
        evaluateIntent(newIntent);
    }

    @Subscribe
    public void onNewDecisionAdded(AddNewDecisionEvent event) {
        Intent newIntent = new Intent(this, this.getClass());
        newIntent.putExtra(INTENT_EXTRA_STORY_ID, event.parentStoryId);
        newIntent.putExtra(INTENT_EXTRA_PREVIOUS_CHAPTER_ID, event.previousChapterId);

        newIntent.setAction(ACTION_NEW_DECISION);
    }

    @Subscribe
    public void onEditingFinished(EditingFinishedEvent event) {
        finish();
    }

    public static class DiscardActionEvent {

        public final boolean isDiscardDecided;

        public DiscardActionEvent(boolean isDiscardDecided) {
            this.isDiscardDecided = isDiscardDecided;
        }
    }

    public static class StartNewChapterEvent {

        @NonNull
        public final String parentStoryId;
        @Nullable
        public final String previousChapterId;
        @Nullable
        public final String previousChoiceId;

        public StartNewChapterEvent(@NonNull String parentStoryId,
                                    @Nullable String previousChapterId,
                                    @Nullable String previousChoiceId) {
            this.parentStoryId = parentStoryId;
            this.previousChapterId = previousChapterId;
            this.previousChoiceId = previousChoiceId;
        }
    }

    public static class AddNewDecisionEvent {

        @NonNull
        public final String parentStoryId;
        @NonNull
        public final String previousChapterId;
        @NonNull
        public final Chapter previousChapter;

        public AddNewDecisionEvent(@NonNull String parentStoryId, @NonNull String previousChapterId, @NonNull Chapter previousChapter) {
            this.parentStoryId = parentStoryId;
            this.previousChapterId = previousChapterId;
            this.previousChapter = previousChapter;
        }
    }

    public static class EditingFinishedEvent {

    }
}