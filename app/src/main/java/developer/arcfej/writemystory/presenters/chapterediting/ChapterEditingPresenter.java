package developer.arcfej.writemystory.presenters.chapterediting;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import developer.arcfej.writemystory.EditingActivity;
import developer.arcfej.writemystory.R;
import developer.arcfej.writemystory.model.AppDataManager;
import developer.arcfej.writemystory.model.DataManager;
import developer.arcfej.writemystory.model.EditTextType;
import developer.arcfej.writemystory.model.database.DatabaseContract;
import developer.arcfej.writemystory.pojos.Chapter;
import developer.arcfej.writemystory.presenters.BasePresenter;
import developer.arcfej.writemystory.services.AutoSaveService;
import developer.arcfej.writemystory.utilities.AnalyticsHelper;
import developer.arcfej.writemystory.views.BaseView;
import developer.arcfej.writemystory.views.chapterediting.ChapterEditingView;
import developer.arcfej.writemystory.views.chapterediting.ChapterEditingViewImpl;

public class ChapterEditingPresenter implements BasePresenter {

    private static final String KEY_STATE_CHAPTER_ID = "chapter_id";
    private static final String KEY_STATE_CHAPTER = "chapter";
    public static final String KEY_STATE_STORY_ID = "story_id";

    private final FirebaseAnalytics firebaseAnalytics;
    private final DataManager dataManager;
    private final Resources resources;

    private String storyId;
    @Nullable
    private String previousChapterId;
    @Nullable
    private String previousChoiceId;
    @Nullable
    private String chapterId;
    @Nullable
    private Chapter chapter;

    @Nullable
    private ChapterEditingView view;

    private boolean isAutoSaveAllowed = true;

    public ChapterEditingPresenter(FirebaseAnalytics firebaseAnalytics, DataManager dataManager,
                                   Resources resources, String storyId,
                                   @Nullable String previousChapterId,
                                   @Nullable String previousChoiceId, Bundle presenterState) {
        this.firebaseAnalytics = firebaseAnalytics;
        this.dataManager = dataManager;
        this.resources = resources;
        this.storyId = storyId;
        this.previousChapterId = previousChapterId;
        this.previousChoiceId = previousChoiceId;

        if (presenterState != null) {
            if (presenterState.containsKey(KEY_STATE_STORY_ID)) {
                this.storyId = presenterState.getString(KEY_STATE_STORY_ID);
            }
            if (presenterState.containsKey(KEY_STATE_CHAPTER_ID)) {
                chapterId = presenterState.getString(KEY_STATE_CHAPTER_ID);
            }
            if (presenterState.containsKey(KEY_STATE_CHAPTER)) {
                chapter = presenterState.getParcelable(KEY_STATE_CHAPTER);
            }
        }
    }

    @Override
    public void attachView(BaseView view) {
        this.view = (ChapterEditingView) view;
        if (chapter == null) {
            this.view.showLoading();
            dataManager.requestChapter(chapterId, storyId, previousChapterId, previousChoiceId);
        } else {
            this.view.hideLoading();
        }
    }

    @Override
    public Bundle getState() {
        Bundle state = new Bundle();
        state.putParcelable(KEY_STATE_CHAPTER, chapter);
        state.putString(KEY_STATE_CHAPTER_ID, chapterId);
        return state;
    }

    @Subscribe
    public void onDiscard(EditingActivity.DiscardActionEvent event) {
        // Delete the drafts if the user clicked 'Discard' on the AlertDialog
        if (event.isDiscardDecided) {
            deleteDrafts();
            EventBus.getDefault().post(new EditingActivity.EditingFinishedEvent());
        }
    }

    private void deleteDrafts() {
        // If story == null, than there is no data to delete. Do nothing
        if (chapter != null && chapterId != null) {
            dataManager.saveDraft(EditTextType.CHAPTER_BODY, null, chapterId);
            Bundle discardParams = new Bundle();
            discardParams.putString(
                    AnalyticsHelper.Param.CONTENT_TYPE, DatabaseContract.DbCollection.CHAPTERS.name()
            );
            firebaseAnalytics.logEvent(AnalyticsHelper.Event.CHAPTER_DISCARDED, discardParams);
        }
        isAutoSaveAllowed = false;
    }

    @Subscribe
    public void onAutoSave(AutoSaveService.AutoSaveEvent event) {
        // There is nowhere or nothing to save. Return
        if (chapter == null || view == null || chapterId == null || !isAutoSaveAllowed) return;
        // Get view's state
        Bundle viewState = view.getViewState();
        final String chapterBody =
                viewState.getString(ChapterEditingView.STATE_CHAPTER_BODY_INPUT, null);
        // Save the text as draft
        dataManager.saveDraft(EditTextType.CHAPTER_BODY, chapterBody, chapterId);
        Bundle autoSaveParams = new Bundle();
        autoSaveParams.putString(
                AnalyticsHelper.Param.CONTENT_TYPE,
                EditTextType.CHAPTER_BODY.name()
        );
        firebaseAnalytics.logEvent(AnalyticsHelper.Event.AUTO_SAVE, autoSaveParams);
    }

    @Subscribe
    public void onSave(ChapterEditingViewImpl.SaveChapterEvent event) {
        // If inputs not valid, show error messages and return
        if (!validateInputText(event.chapterBody)) {
            return;
        }
        saveText(event.chapterBody);
        deleteDrafts();
        EventBus.getDefault().post(new EditingActivity.EditingFinishedEvent());
    }

    @Subscribe
    public void onPublishChapter(ChapterEditingViewImpl.FinishChapterEvent event) {
        // If inputs not valid, show error messages and return
        if (!validateInputText(event.chapterBody)) {
            return;
        }
        // TODO
    }

    @Subscribe
    public void onContinueStory(ChapterEditingViewImpl.AddChoicesEvent event) {
        // If inputs not valid, show error messages and return
        if (!validateInputText(event.chapterBody)) {
            return;
        }
        saveText(event.chapterBody);
        deleteDrafts();
        // Loading is showed during chapter and chapterId equals null. UI's not interactive until.
        assert chapterId != null;
        assert chapter != null;
        EventBus.getDefault().post(new EditingActivity.AddNewDecisionEvent(
                storyId, chapterId, chapter
        ));
    }

    /**
     * @param chapterBody which needs validation
     * @return true if the inputs are valid
     */
    private boolean validateInputText(String chapterBody) {
        // If the text fields are empty, show error messages. If not, save them and continue
        boolean isContinueAllowed = true;
        String chapterBodyError = null;
        if (TextUtils.isEmpty(chapterBody)) {
            chapterBodyError = resources.getString(R.string.error_message_empty_input);
            isContinueAllowed = false;
        }
        // View is not null, because the title and shortDescription are gotten from it.
        assert view != null;
        view.setErrorMessage(chapterBodyError);
        return isContinueAllowed;
    }

    private void saveText(String chapterBody) {
        assert chapterId != null;
        dataManager.saveText(EditTextType.CHAPTER_BODY, chapterBody, chapterId);
        Bundle saveParams = new Bundle();
        saveParams.putSerializable(
                AnalyticsHelper.Param.CONTENT_TYPE,
                EditTextType.CHAPTER_BODY.name()
        );
        firebaseAnalytics.logEvent(AnalyticsHelper.Event.TEXT_SAVE, saveParams);
    }

    @Subscribe
    public void onChapterReady(AppDataManager.ChapterReadyEvent event) {
        chapterId = event.chapterId;
        chapter = event.chapter;
        // Get the drafts or the saved texts from the story, and setup the view with them
        String chapterBody = chapter.chapterBodyDraft;
        if (TextUtils.isEmpty(chapterBody)) {
            chapterBody = chapter.chapterBody;
        }
        if (view != null) {
            view.setInputField(chapterBody);
            view.hideLoading();
        }
    }
}