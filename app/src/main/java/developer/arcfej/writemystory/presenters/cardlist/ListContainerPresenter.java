package developer.arcfej.writemystory.presenters.cardlist;

import developer.arcfej.writemystory.pojos.ListType;
import developer.arcfej.writemystory.presenters.BasePresenter;

public interface ListContainerPresenter extends BasePresenter {

    ListType getListType();

    void setListType(ListType listType);

    void popListTypeStack();
}