package developer.arcfej.writemystory.presenters;

import android.os.Bundle;

import developer.arcfej.writemystory.views.BaseView;

public interface BasePresenter {

    /**
     * Must call in onStart(), when the UI is already visible. Request data in this method.
     * @param view
     */
    void attachView(BaseView view);

    Bundle getState();
}
