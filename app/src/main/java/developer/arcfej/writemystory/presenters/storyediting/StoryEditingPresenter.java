package developer.arcfej.writemystory.presenters.storyediting;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import developer.arcfej.writemystory.R;
import developer.arcfej.writemystory.EditingActivity;
import developer.arcfej.writemystory.model.AppDataManager;
import developer.arcfej.writemystory.model.DataManager;
import developer.arcfej.writemystory.model.EditTextType;
import developer.arcfej.writemystory.model.database.DatabaseContract;
import developer.arcfej.writemystory.pojos.Story;
import developer.arcfej.writemystory.presenters.BasePresenter;
import developer.arcfej.writemystory.services.AutoSaveService;
import developer.arcfej.writemystory.utilities.AnalyticsHelper;
import developer.arcfej.writemystory.views.BaseView;
import developer.arcfej.writemystory.views.storyediting.StoryEditingView;
import developer.arcfej.writemystory.views.storyediting.StoryEditingViewImpl;

public class StoryEditingPresenter implements BasePresenter {

    public static final String KEY_STATE_STORY = "story";
    public static final String KEY_STATE_STORY_ID = "story_id";

    private final FirebaseAnalytics firebaseAnalytics;
    private final DataManager dataManager;
    private final Resources resources;

    // Story is not null during user interactions, because a loading's shown till it gets its value
    @Nullable
    private Story story = null;
    @Nullable
    private String storyId = null;

    @Nullable
    private StoryEditingView view;

    private boolean isAutoSaveAllowed = true;

    public StoryEditingPresenter(FirebaseAnalytics firebaseAnalytics, DataManager dataManager,
                                 Resources resources, Bundle presenterState) {
        this.firebaseAnalytics = firebaseAnalytics;
        this.dataManager = dataManager;
        this.resources = resources;

        if (presenterState != null) {
            if (presenterState.containsKey(KEY_STATE_STORY_ID)) {
                storyId = presenterState.getString(KEY_STATE_STORY_ID);
            }
            if (presenterState.containsKey(KEY_STATE_STORY)) {
                story = presenterState.getParcelable(KEY_STATE_STORY);
            }
        }
    }

    @Override
    public void attachView(BaseView view) {
        this.view = (StoryEditingView) view;
        if (story == null) {
            this.view.showLoading();
            dataManager.requestStory(storyId);
        } else {
            this.view.hideLoading();
        }
    }

    @Override
    public Bundle getState() {
        Bundle state = new Bundle();
        state.putParcelable(KEY_STATE_STORY, story);
        state.putString(KEY_STATE_STORY_ID, storyId);
        return state;
    }

    @Subscribe
    public void onDiscard(EditingActivity.DiscardActionEvent event) {
        // Delete the drafts if the user clicked 'Discard' on the AlertDialog
        if (event.isDiscardDecided) {
            deleteDrafts();
            EventBus.getDefault().post(new EditingActivity.EditingFinishedEvent());
        }
    }

    private void deleteDrafts() {
        // If story == null, than there is no data to delete. Do nothing
        if (story != null && storyId != null) {
            dataManager.saveDraft(EditTextType.TITLE, null, storyId);
            dataManager.saveDraft(EditTextType.SHORT_DESCRIPTION, null, storyId);
            Bundle discardParams = new Bundle();
            discardParams.putString(
                    AnalyticsHelper.Param.CONTENT_TYPE, DatabaseContract.DbCollection.STORIES.name()
            );
            firebaseAnalytics.logEvent(AnalyticsHelper.Event.STORY_DISCARDED, discardParams);
        }
        isAutoSaveAllowed = false;
    }

    @Subscribe
    public void onAutoSave(AutoSaveService.AutoSaveEvent event) {
        // There is nowhere or nothing to save. Return
        if (story == null || view == null || storyId == null || !isAutoSaveAllowed) return;
        // Get view's state
        Bundle viewState = view.getViewState();
        final String title = viewState.getString(StoryEditingView.STATE_TITLE_INPUT, null);
        final String shortDescription =
                viewState.getString(StoryEditingView.STATE_SHORT_DESCRIPTION_INPUT, null);
        // Save the texts as drafts
        dataManager.saveDraft(EditTextType.TITLE, title, storyId);
        dataManager.saveDraft(EditTextType.SHORT_DESCRIPTION, shortDescription, storyId);
        Bundle autoSaveParams = new Bundle();
        autoSaveParams.putString(AnalyticsHelper.Param.CONTENT_TYPE, EditTextType.TITLE.name());
        autoSaveParams.putString(
                AnalyticsHelper.Param.CONTENT_TYPE,
                EditTextType.SHORT_DESCRIPTION.name()
        );
        firebaseAnalytics.logEvent(AnalyticsHelper.Event.AUTO_SAVE, autoSaveParams);
    }

    @Subscribe
    public void onSave(StoryEditingViewImpl.SaveStoryEvent event) {
        // If inputs not valid, show error messages and return
        if (!validateInputTexts(event.title, event.shortDescription)) {
            return;
        }
        saveTexts(event.title, event.shortDescription);
        deleteDrafts();
        EventBus.getDefault().post(new EditingActivity.EditingFinishedEvent());
    }

    @Subscribe
    public void onContinue(final StoryEditingViewImpl.ContinueStoryEvent event) {
        // If inputs not valid, show error messages and return
        if (!validateInputTexts(event.title, event.shortDescription)) {
            return;
        }
        saveTexts(event.title, event.shortDescription);
        deleteDrafts();
        assert storyId != null;
        EventBus.getDefault().post(new EditingActivity.StartNewChapterEvent(
                storyId, null, null
        ));
    }

    /**
     * @param title            which needs validation
     * @param shortDescription which needs validation
     * @return true if the inputs are valid
     */
    private boolean validateInputTexts(String title, String shortDescription) {
        // If the text fields are empty, show error messages. If not, save them and continue
        boolean isContinueAllowed = true;
        String titleError = null;
        String shortDescriptionError = null;
        if (TextUtils.isEmpty(title)) {
            titleError = resources.getString(R.string.error_message_empty_input);
            isContinueAllowed = false;
        }
        if (TextUtils.isEmpty(shortDescription)) {
            shortDescriptionError = resources.getString(R.string.error_message_empty_input);
            isContinueAllowed = false;
        }
        // View is not null, because the title and shortDescription are gotten from it.
        assert view != null;
        view.setErrorMessages(titleError, shortDescriptionError);
        return isContinueAllowed;
    }

    private void saveTexts(String title, String shortDescription) {
        assert storyId != null;
        dataManager.saveText(EditTextType.TITLE, title, storyId);
        dataManager.saveText(EditTextType.SHORT_DESCRIPTION, shortDescription, storyId);
        Bundle saveParams = new Bundle();
        saveParams.putString(AnalyticsHelper.Param.CONTENT_TYPE, EditTextType.TITLE.name());
        saveParams.putString(
                AnalyticsHelper.Param.CONTENT_TYPE,
                EditTextType.SHORT_DESCRIPTION.name()
        );
        firebaseAnalytics.logEvent(AnalyticsHelper.Event.TEXT_SAVE, saveParams);
    }

    @Subscribe
    public void onStoryReady(AppDataManager.StoryReadyEvent event) {
        storyId = event.storyId;
        story = event.story;
        // Get the drafts or the saved texts from the story, and setup the view with them
        String title = story.storyTitleDraft;
        if (TextUtils.isEmpty(title)) {
            title = story.storyTitle;
        }
        String shortDescription = story.shortDescriptionDraft;
        if (TextUtils.isEmpty(shortDescription)) {
            shortDescription = story.shortDescription;
        }
        if (this.view != null) {
            this.view.setInputFields(title, shortDescription);
            view.hideLoading();
        }
    }
}