package developer.arcfej.writemystory.presenters.cardlist;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Arrays;
import java.util.List;

import developer.arcfej.writemystory.ListingActivity;
import developer.arcfej.writemystory.R;
import developer.arcfej.writemystory.model.AppDataManager;
import developer.arcfej.writemystory.model.DataManager;
import developer.arcfej.writemystory.pojos.ListType;
import developer.arcfej.writemystory.pojos.Story;
import developer.arcfej.writemystory.presenters.BasePresenter;
import developer.arcfej.writemystory.views.BaseView;
import developer.arcfej.writemystory.views.cardlist.StoryHolder;

public class CardListAdapter extends RecyclerView.Adapter<StoryHolder> implements BasePresenter {

    private final DataManager dataManager;
    private final Resources resources;
    private ListType listType;

    private List list;

    public CardListAdapter(DataManager dataManager, Resources resources,
                           ListType listType) {
        this.dataManager = dataManager;
        this.resources = resources;
        this.listType = listType;
    }

    @Override
    public void attachView(BaseView view) {
        dataManager.requestStories();
    }

    @Override
    public Bundle getState() {
        return null;
    }

    @NonNull
    @Override
    public StoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new StoryHolder.Builder(parent).build();
    }

    @Override
    public void onBindViewHolder(@NonNull StoryHolder holder, int position) {
        StoryHolder.Builder builder = new StoryHolder.Builder(holder, true);
        switch (listType) {
            // TODO add listeners
            case STORIES:
                final Story story = (Story) list.get(position);
                builder.addTitle(story.storyTitle)
                        .setSubtitle(story.authorId)
                        .setBody(story.shortDescription)
                        .addAction(resources.getString(R.string.action_start), null)
                        .addAction(resources.getString(R.string.action_continue), null)
                        .addCardClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                EventBus.getDefault().post(
                                        new ListingActivity.StartReadingEvent(story, null)
                                );
                            }
                        })
                        .build();
                break;
            case CHAPTERS:
                break;
            case MY_STORIES:
                break;
            case MY_CHAPTERS:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Subscribe
    public void onStoriesReady(AppDataManager.StoriesReadyEvent event) {
        list = Arrays.asList(event.stories);
        notifyDataSetChanged();
    }
}