package developer.arcfej.writemystory.presenters.cardlist;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;

import org.greenrobot.eventbus.Subscribe;

import java.util.Stack;

import developer.arcfej.writemystory.R;
import developer.arcfej.writemystory.pojos.ListType;
import developer.arcfej.writemystory.views.BaseView;
import developer.arcfej.writemystory.views.cardlist.ListContainerView;
import developer.arcfej.writemystory.views.cardlist.ListContainerViewImpl;

public class ListContainerPresenterImpl implements ListContainerPresenter {

    private static final String STATE_LIST_TYPE_STACK = "list_type_stack";

    private Resources resources;

    private Stack<ListType> listTypeStack = new Stack<>();
    private ListContainerView view;

    public ListContainerPresenterImpl(Resources resources, Bundle savedState) {
        this.resources = resources;
        if (savedState != null && savedState.containsKey(STATE_LIST_TYPE_STACK)) {
            listTypeStack = (Stack<ListType>) savedState.getSerializable(STATE_LIST_TYPE_STACK);
        } else listTypeStack.push(ListType.STORIES);
    }

    @Override
    public void attachView(BaseView view) {
        this.view = (ListContainerView) view;
        String upperTitle = null;
        String upperSubtitle = null;
        String lowerTitle = null;
        String lowerSubtitle = null;
        switch (listTypeStack.peek()) {
            case STORIES:
                lowerTitle = resources.getString(R.string.list_header_my_writings);
                this.view.setLowerFragmentVisibility(View.GONE);
                break;
            case CHAPTERS:
                break;
            case MY_STORIES:
                upperTitle = resources.getString(R.string.list_header_stories);
                lowerTitle = resources.getString(R.string.list_header_chapters);
                this.view.setUpperFragmentVisibility(View.VISIBLE);
                this.view.setLowerFragmentVisibility(View.GONE);
                break;
            case MY_CHAPTERS:
                break;
        }
        this.view.setUpperHeader(upperTitle, upperSubtitle, false);
        this.view.setLowerHeader(lowerTitle, lowerSubtitle, true);
    }

    @Override
    public Bundle getState() {
        Bundle state = new Bundle();
        state.putSerializable(STATE_LIST_TYPE_STACK, listTypeStack);
        return state;
    }

    @Override
    public ListType getListType() {
        return listTypeStack.isEmpty() ? ListType.STORIES : listTypeStack.peek();
    }

    @Override
    public void setListType(ListType listType) {
        listTypeStack.push(listType);
    }

    @Override
    public void popListTypeStack() {
        ListType lastListType = listTypeStack.pop();
        if (listTypeStack.isEmpty()) {
            listTypeStack.push(lastListType);
        }
    }

    @Subscribe
    public void onLowerHeaderClick(ListContainerViewImpl.LowerHeaderClickEvent event) {
        switch (listTypeStack.peek()) {
            case STORIES:
                listTypeStack.push(ListType.MY_STORIES);
                view.setUpperHeader(resources.getString(R.string.list_header_stories), null, true);
                view.setLowerHeader(resources.getString(R.string.list_header_chapters), null, true);
                view.setUpperFragmentVisibility(View.VISIBLE);
                view.setLowerFragmentVisibility(View.GONE);
                break;
            case CHAPTERS:
                break;
            case MY_STORIES:
                break;
            case MY_CHAPTERS:
                break;
        }
    }
}
