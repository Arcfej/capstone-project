package developer.arcfej.writemystory.presenters.decisionediting;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import developer.arcfej.writemystory.EditingActivity;
import developer.arcfej.writemystory.model.AppDataManager;
import developer.arcfej.writemystory.model.DataManager;
import developer.arcfej.writemystory.model.EditTextType;
import developer.arcfej.writemystory.model.database.DatabaseContract;
import developer.arcfej.writemystory.pojos.Chapter;
import developer.arcfej.writemystory.pojos.Choice;
import developer.arcfej.writemystory.presenters.BasePresenter;
import developer.arcfej.writemystory.services.AutoSaveService;
import developer.arcfej.writemystory.utilities.AnalyticsHelper;
import developer.arcfej.writemystory.views.BaseView;
import developer.arcfej.writemystory.views.decisionediting.ChoiceHolder;
import developer.arcfej.writemystory.views.decisionediting.DecisionEditingView;

public class DecisionEditingPresenter extends RecyclerView.Adapter<ChoiceHolder> implements BasePresenter {

    private static final String KEY_STATE_CHOICES = "choices";
    private static final String KEY_STATE_STORY_ID = "story_id";
    private static final String KEY_STATE_PREVIOUS_CHAPTER_ID = "previous_chapter_id";
    private static final String KEY_STATE_PREVIOUS_CHAPTER = "previous_chapter";

    private final FirebaseAnalytics firebaseAnalytics;
    private final DataManager dataManager;
    private final Resources resources;

    private String storyId;
    private String previousChapterId;
    @Nullable
    private Chapter previousChapter;
    @Nullable
    private ArrayList<Choice> choices = new ArrayList<>();

    @Nullable
    private DecisionEditingView view;

    private boolean isAutoSaveAllowed = true;

    public DecisionEditingPresenter(FirebaseAnalytics firebaseAnalytics, DataManager dataManager,
                                    Resources resources, String storyId, String previousChapterId,
                                    Bundle presenterState) {
        this.firebaseAnalytics = firebaseAnalytics;
        this.dataManager = dataManager;
        this.resources = resources;

        if (presenterState != null) {
            if (presenterState.containsKey(KEY_STATE_CHOICES)) {
                choices = presenterState.getParcelableArrayList(KEY_STATE_CHOICES);
            }
            if (presenterState.containsKey(KEY_STATE_STORY_ID)) {
                storyId = presenterState.getString(KEY_STATE_STORY_ID);
            }
            if (presenterState.containsKey(KEY_STATE_PREVIOUS_CHAPTER_ID)) {
                previousChapterId = presenterState.getString(KEY_STATE_PREVIOUS_CHAPTER_ID);
            }
            if (presenterState.containsKey(KEY_STATE_PREVIOUS_CHAPTER)) {
                previousChapter = presenterState.getParcelable(KEY_STATE_PREVIOUS_CHAPTER);
            }
        }
    }

    @Override
    public void attachView(BaseView view) {
        this.view = (DecisionEditingView) view;
        this.view.showLoading();
        if (previousChapter == null) {
            dataManager.requestChapter(previousChapterId, storyId, null, null);
        } else {
            this.view.setDecisionText(previousChapter.decisionText);
            if (choices == null) {
                getTheChoices(previousChapter);
            }
        }
    }

    private int choiceRequestCounter = 0;

    private void getTheChoices(@NonNull Chapter chapter) {
        for (String choiceId : chapter.choices) {
            choiceRequestCounter++;
            dataManager.requestChoice(choiceId, storyId, previousChapterId);
        }
    }

    @Override
    public Bundle getState() {
        Bundle state = new Bundle();
        state.putString(KEY_STATE_STORY_ID, storyId);
        state.putString(KEY_STATE_PREVIOUS_CHAPTER_ID, previousChapterId);
        state.putParcelable(KEY_STATE_PREVIOUS_CHAPTER, previousChapter);
        state.putParcelableArrayList(KEY_STATE_CHOICES, choices);
        return state;
    }

    // TODO action buttons + autoSave
    @Subscribe
    public void onDiscard(EditingActivity.DiscardActionEvent event) {
        // Delete the drafts if the user clicked 'Discard' on the AlertDialog
        if (event.isDiscardDecided) {
            deleteDrafts();
            EventBus.getDefault().post(new EditingActivity.EditingFinishedEvent());
        }
    }

    private void deleteDrafts() {
        if (previousChapterId != null && choices != null) {
            dataManager.saveDraft(EditTextType.DECISION_TEXT, null, previousChapterId);
            for (Choice choice : choices) {
                dataManager.saveDraft(EditTextType.CHOICE, null, choice.choiceId);
            }
            Bundle discardParams = new Bundle();
            discardParams.putString(
                    AnalyticsHelper.Param.CONTENT_TYPE, DatabaseContract.DbCollection.CHOICES.name()
            );
            firebaseAnalytics.logEvent(AnalyticsHelper.Event.DECISION_DISCARDED, discardParams);
        }
        isAutoSaveAllowed = false;
    }

    @Subscribe
    public void onAutoSave(AutoSaveService.AutoSaveEvent event) {
        // There is nowhere or nothing to save. Return
        if (previousChapterId == null || view == null || !isAutoSaveAllowed) return;
        // Get view's state
        Bundle viewState = view.getViewState();
        final String decisionText =
                viewState.getString(DecisionEditingView.STATE_DECISION_TEXT, null);
        // Save the decision text as draft
        dataManager.saveDraft(EditTextType.DECISION_TEXT, decisionText, previousChapterId);

        for (int i = 0; i < getItemCount(); i++) {

        }

        Bundle autoSaveParams = new Bundle();
        autoSaveParams.putString(
                AnalyticsHelper.Param.CONTENT_TYPE,
                DatabaseContract.DbCollection.CHOICES.name()
        );
        firebaseAnalytics.logEvent(AnalyticsHelper.Event.AUTO_SAVE, autoSaveParams);
    }

    @NonNull
    @Override
    public ChoiceHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return ChoiceHolder.constructWithInflating(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ChoiceHolder holder, int position) {
        if (getItemCount() - 1 == position) {
            // The last item is an 'add new choice'
            holder.configureButtonClickListener(ChoiceHolder.BUTTON_FUNCTION_ADD, position);
            holder.setInputFieldVisibility(View.GONE);
        } else {
            // getItemCount returns the size of choices + 1, so choices is never null here
            assert choices != null;
            holder.setChoiceText(choices.get(position).choiceText);
            holder.configureButtonClickListener(ChoiceHolder.BUTTON_FUNCTION_REMOVE, position);
            holder.setInputFieldVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return choices == null ? 1 : choices.size() + 1;
    }

    @Subscribe
    public void onPreviousChapterReady(AppDataManager.ChapterReadyEvent event) {
        previousChapterId = event.chapterId;
        previousChapter = event.chapter;
        getTheChoices(previousChapter);
        if (view != null) {
            view.setDecisionText(previousChapter.decisionText);
        }
    }

    @Subscribe
    public void onChoiceReady(AppDataManager.ChoiceReadyEvent event) {
        event.choice.choiceId = event.choiceId;
        choices.add(event.choice);
        choiceRequestCounter--;
        if (view != null && choiceRequestCounter == 0) {
            view.hideLoading();
            notifyDataSetChanged();
        }
    }
}