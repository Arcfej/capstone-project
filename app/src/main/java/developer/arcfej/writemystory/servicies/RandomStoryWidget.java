package developer.arcfej.writemystory.servicies;

import android.annotation.SuppressLint;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RemoteViews;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Random;

import developer.arcfej.writemystory.R;
import developer.arcfej.writemystory.model.AppDataManager;
import developer.arcfej.writemystory.model.DataManager;
import developer.arcfej.writemystory.pojos.Story;

/**
 * Implementation of App Widget functionality.
 */
public class RandomStoryWidget extends AppWidgetProvider {

    public RandomStoryWidget() {

    }

    private static void updateAppWidget(Context context, @Nullable Story story,
                                        AppWidgetManager appWidgetManager, int appWidgetId) {
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_random_story);
        if (story == null) {
            views.setViewVisibility(R.id.pb_loading, View.VISIBLE);
        } else {
            views.setTextViewText(R.id.tv_story_title, story.storyTitle);
            views.setTextViewText(
                    R.id.tv_story_author,
                    context.getString(R.string.story_card_author, story.authorId)
            );
            views.setTextViewText(R.id.tv_story_short_description, story.shortDescription);
            views.setViewVisibility(R.id.pb_loading, View.GONE);
        }

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, null, appWidgetManager, appWidgetId);
        }

        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
        Job queryRandomStory = dispatcher.newJobBuilder()
                .setService(RandomStoryService.class)
                .setTag("query_random_story")
                .build();
        dispatcher.mustSchedule(queryRandomStory);
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    @SuppressLint("NewApi")
    public static class RandomStoryService extends JobService {

        private DataManager dataManager;
        private JobParameters startedJob;

        @Override
        public boolean onStartJob(JobParameters job) {
            FirebaseAnalytics firebaseAnalytics =
                    FirebaseAnalytics.getInstance(getApplicationContext());

            dataManager = AppDataManager.getInstance(firebaseAnalytics);
            dataManager.registerObservers();
            EventBus.getDefault().register(this);

            startedJob = job;
            dataManager.requestStories();
            return true;
        }

        @Override
        public boolean onStopJob(JobParameters job) {
            return true;
        }

        @Subscribe
        public void onQueryReady(AppDataManager.StoriesReadyEvent event) {
            if (EventBus.getDefault().isRegistered(this)) {
                dataManager.unregisterObservers();
                EventBus.getDefault().unregister(this);
            }

            Random random = new Random();

            Context context = getApplicationContext();
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(getApplication());
            ComponentName component = new ComponentName(getApplication(), RandomStoryWidget.class);
            // There may be multiple widgets active, so update all of them
            for (int appWidgetId : appWidgetManager.getAppWidgetIds(component)) {
                int randomIndex = random.nextInt(event.stories.length);
                Story randomStory = event.stories[randomIndex];
                updateAppWidget(context, randomStory, appWidgetManager, appWidgetId);
            }
            jobFinished(startedJob, false);
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            if (EventBus.getDefault().isRegistered(this)) {
                dataManager.unregisterObservers();
                EventBus.getDefault().unregister(this);
            }
        }
    }
}