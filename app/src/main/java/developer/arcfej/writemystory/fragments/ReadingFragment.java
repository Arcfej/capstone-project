package developer.arcfej.writemystory.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import developer.arcfej.writemystory.model.AppDataManager;
import developer.arcfej.writemystory.model.DataManager;
import developer.arcfej.writemystory.pojos.Chapter;
import developer.arcfej.writemystory.pojos.Story;
import developer.arcfej.writemystory.views.BaseView;
import developer.arcfej.writemystory.views.reading.ReadingViewImpl;
import developer.arcfej.writemystory.views.reading.StoryInfoView;
import developer.arcfej.writemystory.views.reading.StoryInfoViewImpl;

public class ReadingFragment extends Fragment {

    // You must pass only one of the arguments to this fragment
    public static final String ARGUMENT_STORY = "story";
    public static final String ARGUMENT_CHAPTER = "chapter";

    private static final String VIEW_STATE = "view_state";

    private FirebaseAnalytics firebaseAnalytics;
    private DataManager dataManager;

    private BaseView view;

    @Nullable
    private Story story;
    @Nullable
    private Chapter chapter;
    private Bundle viewState;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        firebaseAnalytics = FirebaseAnalytics.getInstance(context);
        dataManager = AppDataManager.getInstance(firebaseAnalytics);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            if (args.containsKey(ARGUMENT_STORY)) story = args.getParcelable(ARGUMENT_STORY);
            if (args.containsKey(ARGUMENT_CHAPTER)) chapter = args.getParcelable(ARGUMENT_CHAPTER);
        }
        if (savedInstanceState != null) {
            viewState = savedInstanceState.getBundle(VIEW_STATE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (story != null) {
            view = new StoryInfoViewImpl(
                    inflater, container, story.storyTitle, story.authorId,
                    story.shortDescription, !TextUtils.isEmpty(story.firstChapterId), viewState);
        } else if (chapter != null) {
            view = new ReadingViewImpl(inflater, container, chapter.chapterBody,
                    !TextUtils.isEmpty(chapter.previousChapterId),
                    !TextUtils.isEmpty(chapter.nextChapterId),
                    viewState);
        } else {
            return null;
        }
        return view.getRootView();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (view instanceof StoryInfoViewImpl || view instanceof ReadingViewImpl) {
            firebaseAnalytics.setCurrentScreen(
                    getActivity(),
                    view.getClass().getSimpleName(),
                    this.getClass().getSimpleName()
            );
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        viewState = view.getViewState();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle(VIEW_STATE, view.getViewState());
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onFirstChapterButtonClicked(StoryInfoViewImpl.FirstChapterButtonClickEvent event) {
        if (story != null) {
            dataManager.requestChapter(story.firstChapterId, story.storyId, null, null);
            ((StoryInfoView) view).showLoading();
        }
    }

    @Subscribe
    public void onNextChapterButtonClicked(ReadingViewImpl.NextChapterButtonClickEvent event) {
        if (chapter != null && !TextUtils.isEmpty(chapter.nextChapterId)) {
            dataManager.requestChapter(chapter.nextChapterId, chapter.storyId, chapter.chapterId, chapter.previousChoiceId);
        }
    }

    @Subscribe
    public void onPreviousChapterButtonClicked(ReadingViewImpl.PreviousChapterButtonClickEvent event) {
        if (chapter != null) {
            dataManager.requestChapter(chapter.previousChapterId, chapter.storyId, null, null);
        }
    }
}