package developer.arcfej.writemystory.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.greenrobot.eventbus.EventBus;

import developer.arcfej.writemystory.model.AppDataManager;
import developer.arcfej.writemystory.model.DataManager;
import developer.arcfej.writemystory.pojos.ListType;
import developer.arcfej.writemystory.presenters.BasePresenter;
import developer.arcfej.writemystory.presenters.cardlist.CardListAdapter;
import developer.arcfej.writemystory.views.BaseView;
import developer.arcfej.writemystory.views.cardlist.CardListViewImpl;

public class CardListFragment extends Fragment {

    public static final String ARGUMENT_LIST_TYPE = "list_type";
    public static final String STATE_LIST_TYPE = "list_type";

    private FirebaseAnalytics firebaseAnalytics;
    private DataManager dataManager;

    private ListType listType;
    private BasePresenter presenter;
    private BaseView view;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        firebaseAnalytics = FirebaseAnalytics.getInstance(context);
        dataManager = AppDataManager.getInstance(firebaseAnalytics);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (savedInstanceState != null && savedInstanceState.containsKey(STATE_LIST_TYPE)) {
            listType = (ListType) savedInstanceState.getSerializable(STATE_LIST_TYPE);
        } else if (args != null && args.containsKey(ARGUMENT_LIST_TYPE)) {
            listType = (ListType) args.getSerializable(ARGUMENT_LIST_TYPE);
        } else {
            throw new IllegalArgumentException("The CardListFragment needs an argument with a ListType");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        presenter = new CardListAdapter(dataManager, getResources(), listType);
        view = new CardListViewImpl(inflater, container, (RecyclerView.Adapter) presenter);
        return view.getRootView();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(presenter);
        presenter.attachView(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        firebaseAnalytics.setCurrentScreen(
                getActivity(),
                listType.name(),
                this.getClass().getSimpleName()
        );
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(STATE_LIST_TYPE, listType);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(presenter);
    }
}