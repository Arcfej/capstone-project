package developer.arcfej.writemystory.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.google.firebase.analytics.FirebaseAnalytics;

import developer.arcfej.writemystory.model.AppDataManager;
import developer.arcfej.writemystory.model.DataManager;
import developer.arcfej.writemystory.presenters.BasePresenter;
import developer.arcfej.writemystory.views.BaseView;

public class DecisionEditingFragment extends Fragment {

    public static final String ARGUMENT_STORY_ID = "story_id";
    public static final String ARGUMENT_PREVIOUS_CHAPTER_ID = "previous_chapter_id";

    private static final String STATE_PRESENTER_STATE = "presenter_state";

    private static final String TAG_JOB_AUTO_SAVE = "auto_save";

    private FirebaseAnalytics firebaseAnalytics;
    private DataManager dataManager;
    private FirebaseJobDispatcher jobDispatcher;
    private Job autoSave;

    private BaseView view;
    private BasePresenter presenter;
    private Bundle presenterState;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        firebaseAnalytics = FirebaseAnalytics.getInstance(context);
        dataManager = AppDataManager.getInstance(firebaseAnalytics);
        jobDispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
    }
}