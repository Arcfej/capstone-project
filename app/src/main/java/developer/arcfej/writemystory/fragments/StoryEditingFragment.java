package developer.arcfej.writemystory.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.greenrobot.eventbus.EventBus;

import developer.arcfej.writemystory.model.AppDataManager;
import developer.arcfej.writemystory.model.DataManager;
import developer.arcfej.writemystory.presenters.BasePresenter;
import developer.arcfej.writemystory.presenters.storyediting.StoryEditingPresenter;
import developer.arcfej.writemystory.services.AutoSaveService;
import developer.arcfej.writemystory.views.BaseView;
import developer.arcfej.writemystory.views.storyediting.StoryEditingViewImpl;

public class StoryEditingFragment extends Fragment {

    public static final String ARGUMENT_EXISTING_STORY_ID = "story_id";

    private static final String STATE_PRESENTER_STATE = "presenter_state";
    private static final String STATE_VIEW_STATE = "view_state";

    private static final String TAG_JOB_AUTO_SAVE = "auto_save";

    private FirebaseAnalytics firebaseAnalytics;
    private DataManager dataManager;
    private FirebaseJobDispatcher jobDispatcher;
    private Job autoSave;

    private BaseView view;
    private BasePresenter presenter;
    private Bundle presenterState;
    private Bundle viewState;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        firebaseAnalytics = FirebaseAnalytics.getInstance(context);
        dataManager = AppDataManager.getInstance(firebaseAnalytics);
        jobDispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(STATE_PRESENTER_STATE)) {
                presenterState = savedInstanceState.getBundle(STATE_PRESENTER_STATE);
            } else {
                Bundle args = getArguments();
                if (args != null) {
                    presenterState = new Bundle();
                    presenterState.putString(
                            StoryEditingPresenter.KEY_STATE_STORY_ID,
                            args.getString(ARGUMENT_EXISTING_STORY_ID, null)
                    );
                }
            }
            if (savedInstanceState.containsKey(STATE_VIEW_STATE)) {
                viewState = savedInstanceState.getBundle(STATE_VIEW_STATE);
            }
        }
        autoSave = jobDispatcher.newJobBuilder()
                .setService(AutoSaveService.class)
                .setTag(TAG_JOB_AUTO_SAVE)
                .setRecurring(true)
                .setTrigger(Trigger.executionWindow(60, 90))
                .setReplaceCurrent(false)
                .setRetryStrategy(RetryStrategy.DEFAULT_LINEAR)
                .setLifetime(Lifetime.UNTIL_NEXT_BOOT)
                .build();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        presenter = new StoryEditingPresenter(
                firebaseAnalytics, dataManager, getResources(), presenterState
        );
        view = new StoryEditingViewImpl(inflater, container, viewState);
        return view.getRootView();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(presenter);
        presenter.attachView(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        firebaseAnalytics.setCurrentScreen(
                getActivity(),
                "STORY_EDITING",
                this.getClass().getSimpleName()
        );
        jobDispatcher.mustSchedule(autoSave);
    }

    @Override
    public void onPause() {
        super.onPause();
        presenterState = presenter.getState();
        viewState = view.getViewState();
        EventBus.getDefault().post(new AutoSaveService.AutoSaveEvent());
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle(STATE_PRESENTER_STATE, presenter.getState());
        outState.putBundle(STATE_VIEW_STATE, view.getViewState());
    }

    @Override
    public void onStop() {
        super.onStop();
        jobDispatcher.cancel(TAG_JOB_AUTO_SAVE);
        EventBus.getDefault().unregister(presenter);
    }
}