package developer.arcfej.writemystory.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.greenrobot.eventbus.EventBus;

import developer.arcfej.writemystory.model.AppDataManager;
import developer.arcfej.writemystory.model.DataManager;
import developer.arcfej.writemystory.presenters.BasePresenter;
import developer.arcfej.writemystory.presenters.chapterediting.ChapterEditingPresenter;
import developer.arcfej.writemystory.services.AutoSaveService;
import developer.arcfej.writemystory.views.BaseView;
import developer.arcfej.writemystory.views.chapterediting.ChapterEditingViewImpl;

public class ChapterEditingFragment extends Fragment {

    public static final String ARGUMENT_STORY_ID = "story_id";
    public static final String ARGUMENT_PREVIOUS_CHAPTER_ID = "previous_chapter_id";
    public static final String ARGUMENT_PREVIOUS_CHOICE_ID = "previous_choice_id";

    private static final String STATE_PRESENTER_STATE = "presenter_state";
    private static final String STATE_VIEW_STATE = "view_state";
    private static final String STATE_STORY_ID = "story_id";
    private static final String STATE_PREVIOUS_CHAPTER_ID = "previous_chapter_id";

    private static final String TAG_JOB_AUTO_SAVE = "auto_save";

    private FirebaseAnalytics firebaseAnalytics;
    private DataManager dataManager;
    private FirebaseJobDispatcher jobDispatcher;
    private Job autoSave;

    private BaseView view;
    private BasePresenter presenter;
    private String storyId;
    private String previousChapterId;
    private Bundle presenterState;
    private Bundle viewState;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        firebaseAnalytics = FirebaseAnalytics.getInstance(context);
        dataManager = AppDataManager.getInstance(firebaseAnalytics);
        jobDispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            if (args.containsKey(ARGUMENT_STORY_ID)) {
                storyId = args.getString(ARGUMENT_STORY_ID, null);
            }
            if (args.containsKey(ARGUMENT_PREVIOUS_CHAPTER_ID)) {
                previousChapterId = args.getString(ARGUMENT_PREVIOUS_CHAPTER_ID);
            }
        }

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(STATE_PRESENTER_STATE)) {
                presenterState = savedInstanceState.getBundle(STATE_PRESENTER_STATE);
            }
            if (savedInstanceState.containsKey(STATE_STORY_ID)) {
                storyId = savedInstanceState.getString(STATE_STORY_ID);
            }
            if (savedInstanceState.containsKey(STATE_PREVIOUS_CHAPTER_ID)) {
                previousChapterId = savedInstanceState.getString(STATE_PREVIOUS_CHAPTER_ID);
            }
            if (savedInstanceState.containsKey(STATE_VIEW_STATE)) {
                viewState = savedInstanceState.getBundle(STATE_VIEW_STATE);
            }
        }

        if (TextUtils.isEmpty(storyId)) {
            throw new IllegalArgumentException("This fragment has to get a storyId through its arguments");
        }

        autoSave = jobDispatcher.newJobBuilder()
                .setService(AutoSaveService.class)
                .setTag(TAG_JOB_AUTO_SAVE)
                .setRecurring(true)
                .setTrigger(Trigger.executionWindow(60, 90))
                .setReplaceCurrent(false)
                .setRetryStrategy(RetryStrategy.DEFAULT_LINEAR)
                .setLifetime(Lifetime.UNTIL_NEXT_BOOT)
                .build();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        presenter = new ChapterEditingPresenter(firebaseAnalytics, dataManager, getResources(),
                storyId, previousChapterId, null, presenterState);
        view = new ChapterEditingViewImpl(inflater, container, viewState);
        return view.getRootView();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(presenter);
        presenter.attachView(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        firebaseAnalytics.setCurrentScreen(
                getActivity(),
                "CHAPTER_EDITING",
                this.getClass().getSimpleName()
        );
        jobDispatcher.mustSchedule(autoSave);
    }

    @Override
    public void onPause() {
        super.onPause();
        presenterState = presenter.getState();
        viewState = view.getViewState();
        EventBus.getDefault().post(new AutoSaveService.AutoSaveEvent());
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle(STATE_PRESENTER_STATE, presenter.getState());
        outState.putString(STATE_STORY_ID, storyId);
        outState.putString(STATE_PREVIOUS_CHAPTER_ID, previousChapterId);
        outState.putBundle(STATE_VIEW_STATE, view.getViewState());
    }

    @Override
    public void onStop() {
        super.onStop();
        jobDispatcher.cancel(TAG_JOB_AUTO_SAVE);
        EventBus.getDefault().unregister(presenter);
    }
}