package developer.arcfej.writemystory.services;

import android.annotation.SuppressLint;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

import org.greenrobot.eventbus.EventBus;

@SuppressLint("NewApi")
public class AutoSaveService extends JobService {

    @Override
    public boolean onStartJob(JobParameters job) {
        EventBus.getDefault().post(new AutoSaveEvent());
        jobFinished(job, true);
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        return false;
    }

    public static class AutoSaveEvent {

    }
}
