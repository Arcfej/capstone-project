package developer.arcfej.writemystory.utilities;

import com.google.firebase.analytics.FirebaseAnalytics;

public class AnalyticsHelper {

    public static class Event extends FirebaseAnalytics.Event {

        public static final String AUTO_SAVE = "auto_save";
        public static final String CHAPTER_DISCARDED = "chapter_discarded";
        public static final String DECISION_DISCARDED = "decision_discarded";
        public static final String LOGIN_REFUSED = "login_refused";
        public static final String LOGOUT = "logout";
        public static final String NEW_STORY_CREATED = "new_story_created";
        public static final String NEW_USER = "new_user";
        public static final String STORY_DISCARDED = "story_discarded";
        public static final String TEXT_SAVE = "text_save";
    }

    public static class Param extends FirebaseAnalytics.Param {

    }
}