package developer.arcfej.writemystory.utilities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.greenrobot.eventbus.EventBus;

public class NetworkChangeReceiver extends BroadcastReceiver{

    private static NetworkChangeReceiver instance = null;

    public static synchronized NetworkChangeReceiver getInstance() {
        if (instance == null) {
            instance = new NetworkChangeReceiver();
        }
        return instance;
    }

    private final IntentFilter intentFilter;

    private NetworkChangeReceiver() {
        intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        EventBus.getDefault().post(new NetworkStateChangeEvent(isConnected(context)));
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager manager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = manager.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    public IntentFilter getIntentFilter() {
        return intentFilter;
    }

    public static class NetworkStateChangeEvent {

        public final boolean isConnected;

        public NetworkStateChangeEvent(boolean isConnected) {
            this.isConnected = isConnected;
        }
    }
}