package developer.arcfej.writemystory.model.database;

// If you rename any constant, the field name must be updated in the pojo classes too!!!
// (User, Story, Chapter, Choice, etc)
public class DatabaseContract {

    /**
     * database
     * |    users []
     * |    |   userId
     * |    |   |   stories []
     * |    |   |   |   storyId
     * |    |   |   chapters []
     * |    |   |   |   chapterId
     * |    |   |   name
     * |    |   |   email
     * |    |   |   isAuthenticated
     * |    stories []
     * |    |   storyId
     * |    |   |   authorId
     * |    |   |   shortDescription
     * |    |   |   shortDescriptionDraft
     * |    |   |   firstChapterId
     * |    |   |   contributors []
     * |    |   |   |   contributorId
     * |    |   |   chaptersCount
     * |    |   |   longestChain
     * |    |   |   storyTitle
     * |    |   |   storyTitleDraft
     * |    |   |   timestamp
     * |    |   |   isPublished
     * |    chapters []
     * |    |   chapterId
     * |    |   |   authorId
     * |    |   |   storyId
     * |    |   |   previousChapterId
     * |    |   |   nextChapterId
     * |    |   |   previousChoiceId
     * |    |   |   depth
     * |    |   |   longestChain
     * |    |   |   choices []
     * |    |   |   |   choiceId
     * |    |   |   chapterBody
     * |    |   |   chapterBodyDraft
     * |    |   |   decisionText
     * |    |   |   timestamp
     * |    |   |   isPublished
     * |    choices []
     * |    |   choiceId
     * |    |   |   authorId
     * |    |   |   storyId
     * |    |   |   previousChapterId
     * |    |   |   chapters []
     * |    |   |   |   chapterId
     * |    |   |   choiceText
     * |    |   |   decisionText
     * |    |   |   isPublished
     */

    public enum DbCollection {
        USERS("users"),
        STORIES("stories"),
        CHAPTERS("chapters"),
        CHOICES("choices");

        private final String tableName;

        DbCollection(String tableName) {
            this.tableName = tableName;
        }

        public String getCollectionName() {
            return tableName;
        }
    }

    public enum DbKey {
        AUTHOR_ID("authorId"),
        CHAPTER_BODY("chapterBody"),
        CHAPTER_BODY_DRAFT("chapterBodyDraft"),
        CHAPTERS("chapters"),
        CHAPTERS_COUNT("chaptersCount"),
        CHOICES("choices"),
        CHOICE_TEXT("choiceText"),
        CONTRIBUTORS("contributors"),
        DECISION_TEXT("decisionText"),
        DEPTH("depth"),
        EMAIL("email"),
        FIRST_CHAPTER_ID("firstChapterId"),
        LONGEST_CHAIN("longestChain"),
        NAME("name"),
        NEXT_CHAPTER_ID("nextChapterId"),
        PREVIOUS_CHAPTER_ID("previousChapterId"),
        PREVIOUS_CHOICE_ID("previousChoiceId"),
        IS_AUTHENTICATED("isAuthenticated"),
        IS_PUBLISHED("isPublished"),
        SHORT_DESCRIPTION("shortDescription"),
        SHORT_DESCRIPTION_DRAFT("shortDescriptionDraft"),
        STORIES("stories"),
        STORY_ID("storyId"),
        TIMESTAMP("timestamp"),
        STORY_TITLE("storyTitle"),
        STORY_TITLE_DRAFT("storyTitleDraft");

        private final String keyName;

        DbKey(String keyName) {
            this.keyName = keyName;
        }

        public String getKeyName() {
            return keyName;
        }
    }
}