package developer.arcfej.writemystory.model.authenticate;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;

import org.greenrobot.eventbus.EventBus;

import developer.arcfej.writemystory.model.AppDataManager;
import developer.arcfej.writemystory.pojos.User;
import developer.arcfej.writemystory.utilities.AnalyticsHelper;

public class FirebaseAuthenticateHelper implements AuthenticateHelper {

    private static FirebaseAuthenticateHelper instance = null;

    public static synchronized FirebaseAuthenticateHelper getInstance(
            FirebaseAnalytics firebaseAnalytics
    ) {
        if (instance == null) {
            synchronized (AppDataManager.class) {
                instance = new FirebaseAuthenticateHelper(firebaseAnalytics);
            }
        }
        return instance;
    }

    private final FirebaseAnalytics firebaseAnalytics;
    private FirebaseAuth auth;

    private FirebaseAuthenticateHelper(FirebaseAnalytics firebaseAnalytics) {
        this.firebaseAnalytics = firebaseAnalytics;
        auth = FirebaseAuth.getInstance();
        checkUserValidity();
    }

    /**
     * Check, if the user has been disabled, deleted,
     * or its credentials are no longer valid.
     * This code needs to be run only once, when the application started.
     */
    private void checkUserValidity() {
        if (auth.getCurrentUser() != null) {
            final Task<GetTokenResult> task = auth.getCurrentUser().getIdToken(true);
            task.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    auth.signOut();
                    firebaseAnalytics.logEvent(AnalyticsHelper.Event.LOGOUT, null);
                    // TODO subscribe in every activity
                    EventBus.getDefault().post(new LogoutEvent());
                }
            });
        }
    }

    @Override
    public boolean isUserLoggedIn() {
        return auth.getCurrentUser() != null;
    }

    @Override
    public boolean isUserAuthenticated() {
        return auth.getCurrentUser() != null && !auth.getCurrentUser().isAnonymous();
    }

    @Nullable
    @Override
    public User getCurrentUser() {
        FirebaseUser currentUser = auth.getCurrentUser();
        if (currentUser == null) {
            return null;
        } else {
            return new User(
                    currentUser.getDisplayName(), currentUser.getEmail(), !currentUser.isAnonymous()
            );
        }
    }

    @Override
    public String getCurrentUserId() {
        return auth.getUid();
    }

    public static class LogoutEvent {

    }
}
