@file:JvmName("FirestoreDatabaseHelper")
package developer.arcfej.writemystory.model.database

import android.text.TextUtils
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Transaction
import developer.arcfej.writemystory.model.AppDataManager
import developer.arcfej.writemystory.model.database.DatabaseContract.DbCollection
import developer.arcfej.writemystory.model.database.DatabaseContract.DbKey
import developer.arcfej.writemystory.pojos.Chapter
import developer.arcfej.writemystory.pojos.Choice
import developer.arcfej.writemystory.pojos.Story
import developer.arcfej.writemystory.pojos.User
import developer.arcfej.writemystory.utilities.SingletonHolder
import org.greenrobot.eventbus.EventBus
import java.util.*

class FirestoreDatabaseHelper private constructor(parameters: Parameters) : DatabaseHelper {

    data class Parameters(val firebaseAnalytics: FirebaseAnalytics, val modelBus: EventBus)

    companion object : SingletonHolder<Parameters, FirestoreDatabaseHelper>(::FirestoreDatabaseHelper)

    private val firebaseAnalytics: FirebaseAnalytics = parameters.firebaseAnalytics
    private val modelBus: EventBus = parameters.modelBus
    private val database: FirebaseFirestore = FirebaseFirestore.getInstance()

    override fun addNewUser(userId: String, user: User) {
        val userValues = user.toMap()

        database.batch()
                .set(database.collection(DbCollection.USERS.collectionName).document(userId),
                        userValues)
                .commit()
    }

    override fun addNewStory(story: Story) {
        database.collection(DbCollection.STORIES.collectionName)
                .add(story.toMap())
                .addOnSuccessListener { _ ->
                    OnSuccessListener<DocumentReference> { newStory ->
                        database.collection(DbCollection.USERS.collectionName)
                                .document(story.authorId)
                                .update(DbKey.STORIES.keyName, FieldValue.arrayUnion(newStory.id))
                                .addOnSuccessListener { _ ->
                                    OnSuccessListener<DocumentReference> {
                                        EventBus.getDefault().postSticky(
                                                AppDataManager.StoryReadyEvent(newStory.id, story))
                                    }
                                }
                    }
                }
    }

    override fun updateStory(storyId: String, storyValues: Map<DbKey, Any>) {
        val values = HashMap<String, Any>()
        for ((key, value) in storyValues) {
            values[key.keyName] = value
        }
        database.collection(DbCollection.STORIES.collectionName)
                .document(storyId)
                .update(values)
    }

    override fun addNewChapter(chapter: Chapter) {
        database.collection(DbCollection.CHAPTERS.collectionName)
                .add(chapter.toMap())
                .addOnSuccessListener { _ ->
                    OnSuccessListener<DocumentReference> { newChapter ->
                        database.runTransaction(Transaction.Function<Void> { transaction ->
                            newChapterTransaction(transaction, chapter, newChapter)
                            return@Function null
                        })
                                .addOnSuccessListener { _ ->
                                    modelBus.postSticky(
                                            AppDataManager.ChapterReadyEvent(newChapter.id, chapter))
                                }
                                .addOnFailureListener { _ ->
                                    database.collection(DbCollection.CHAPTERS.collectionName)
                                            .document(newChapter.id)
                                            .delete()
                                }
                    }
                }
    }

    private fun newChapterTransaction(transaction: Transaction,
                                      chapter: Chapter,
                                      newChapterReference: DocumentReference) {
        var storySnapshot = transaction.get(
                database.collection(DbCollection.STORIES.collectionName)
                        .document(chapter.storyId))
        // Add the chapter to the author's profile
        transaction.update(database.collection(DbCollection.USERS.collectionName)
                .document(chapter.authorId),
                DbKey.CHAPTERS.keyName,
                FieldValue.arrayUnion(newChapterReference.id))
        val firstChapterId = storySnapshot.getString(DbKey.FIRST_CHAPTER_ID.keyName)
        if (TextUtils.isEmpty(firstChapterId)) {
            // Set this chapter as the story's first one
            transaction.update(database.collection(DbCollection.STORIES.collectionName)
                    .document(chapter.storyId),
                    DbKey.FIRST_CHAPTER_ID.keyName,
                    newChapterReference.id)
        } else {
            // Add this chapter's id to the previous one
            transaction.update(database.collection(DbCollection.CHAPTERS.collectionName)
                    .document(chapter.previousChapterId),
                    DbKey.NEXT_CHAPTER_ID.keyName,
                    newChapterReference.id)
        }
    }

    override fun updateChapter(chapterId: String, chapterValues: Map<DbKey, Any>) {
        val values = HashMap<String, Any>()
        for ((key, value) in chapterValues) {
            values[key.keyName] = value
        }
        database.collection(DbCollection.CHAPTERS.collectionName)
                .document(chapterId)
                .update(values)
    }

    fun publishChapter() {
        //        storyRef.addListenerForSingleValueEvent(new ValueEventListener() {
        //            @Override
        //            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        //                Story story = dataSnapshot.getValue(Story.class);
        //                if (story == null) return;
        //                // Add the author to the story's contributor list (no duplicate)
        //                if (!story.contributors.contains(chapter.authorId)) {
        //                    storyRef.child(DbKey.CONTRIBUTORS.getKeyName())
        //                            .child(chapter.authorId)
        //                            .setValue(true);
        //                }
        //            }
        //
        //            @Override
        //            public void onCancelled(@NonNull DatabaseError databaseError) {
        //
        //            }
        //        });
        //
        //        // Set longest chain and depth, update previous chapters and the story according to this
        //        if (chapter.previousChapterId != null) {
        //            DatabaseReference previousChapterRef =
        //                    database.child(DbCollection.CHAPTERS.getTableName()).child(chapter.authorId);
        //            previousChapterRef.child(DbKey.DEPTH.getKeyName())
        //                    .addListenerForSingleValueEvent(new ValueEventListener() {
        //                        @Override
        //                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        //
        //                        }
        //
        //                        @Override
        //                        public void onCancelled(@NonNull DatabaseError databaseError) {
        //
        //                        }
        //                    });
        //        }
    }

    override fun addNewChoice(choice: Choice) {
        database.collection(DbCollection.CHOICES.collectionName)
                .add(choice.toMap())
                .addOnSuccessListener { _ ->
                    OnSuccessListener<DocumentReference> { newChoice ->
                        database.collection(DbCollection.CHAPTERS.collectionName)
                                .document(choice.previousChapterId)
                                .update(DbKey.CHOICES.keyName, FieldValue.arrayUnion(newChoice.id))
                    }
                }
    }

    override fun updateChoice(choiceId: String, choiceValues: Map<DbKey, Any>) {
        val values = HashMap<String, Any>()
        for ((key, value) in choiceValues) {
            values[key.keyName] = value
        }
        database.collection(DbCollection.CHAPTERS.collectionName)
                .document(choiceId)
                .update(values)
    }

    override fun queryPublishedStories() {
        database.collection(DbCollection.STORIES.collectionName)
                .whereEqualTo(DbKey.IS_PUBLISHED.keyName, true)
                .get()
                .addOnCompleteListener { task ->
                    //                    val stories: Array<Story?>
//                    if (task.isSuccessful) {
//                        for (document: QueryDocumentSnapshot in task.result) {
//
//                        }
//                    }
                    val stories: Array<Story?> = if (task.isSuccessful)
                        Array(task.result.size()) { i ->
                            val story = task.result.documents[i].toObject(Story::class.java)
                            story?.storyId = task.result.documents[i].id
                            return@Array story
                        }
                    else arrayOf()
                    modelBus.postSticky(AppDataManager.StoriesReadyEvent(stories))
                }
    }

    override fun querySingleStory(storyId: String) {
        database.collection(DbCollection.STORIES.collectionName)
                .document(storyId)
                .get()
                .addOnCompleteListener { task ->
                    val story = if (task.isSuccessful) task.result.toObject(Story::class.java)
                    else null
                    story?.storyId = task.result.id
                    modelBus.postSticky(AppDataManager.StoryReadyEvent(storyId, story))
                }
    }

    override fun querySingleChapter(chapterId: String) {
        database.collection(DbCollection.CHAPTERS.collectionName)
                .document(chapterId)
                .get()
                .addOnCompleteListener { task ->
                    val chapter = if (task.isSuccessful) task.result.toObject(Chapter::class.java)
                    else null
                    chapter?.chapterId = task.result.id
                    modelBus.postSticky(AppDataManager.ChapterReadyEvent(chapterId, chapter))
                }
    }

    override fun querySingleChoice(choiceId: String) {
        database.collection(DbCollection.CHOICES.collectionName)
                .document(choiceId)
                .get()
                .addOnCompleteListener { task ->
                    val choice = if (task.isSuccessful) task.result.toObject(Choice::class.java)
                    else null
                    choice?.choiceId = task.result.id
                    modelBus.postSticky(AppDataManager.ChoiceReadyEvent(choiceId, choice))
                }
    }
}