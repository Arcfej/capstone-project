package developer.arcfej.writemystory.model;

public enum EditTextType {
    TITLE,
    SHORT_DESCRIPTION,
    CHAPTER_BODY,
    DECISION_TEXT,
    CHOICE
}