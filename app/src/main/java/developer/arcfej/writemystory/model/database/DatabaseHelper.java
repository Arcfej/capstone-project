package developer.arcfej.writemystory.model.database;

import android.support.annotation.NonNull;

import java.util.Map;

import developer.arcfej.writemystory.pojos.Chapter;
import developer.arcfej.writemystory.pojos.Choice;
import developer.arcfej.writemystory.pojos.Story;
import developer.arcfej.writemystory.pojos.User;

public interface DatabaseHelper {

    void addNewUser(@NonNull String userId, @NonNull User user);

    void addNewStory(@NonNull Story story);

    void updateStory(@NonNull String storyId, Map<DatabaseContract.DbKey, Object> values);

    void addNewChapter(@NonNull Chapter chapter);

    void updateChapter(@NonNull String chapterId, Map<DatabaseContract.DbKey, Object> values);

    void addNewChoice(@NonNull Choice choice);

    void updateChoice(@NonNull String choiceId, Map<DatabaseContract.DbKey, Object> values);

    void queryPublishedStories();

    void querySingleStory(String storyId);

    void querySingleChapter(String chapterId);

    void querySingleChoice(String choiceId);
}