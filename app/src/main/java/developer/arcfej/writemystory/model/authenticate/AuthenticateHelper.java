package developer.arcfej.writemystory.model.authenticate;

import developer.arcfej.writemystory.pojos.User;

public interface AuthenticateHelper {

    boolean isUserLoggedIn();

    boolean isUserAuthenticated();

    User getCurrentUser();

    String getCurrentUserId();
}