package developer.arcfej.writemystory.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import developer.arcfej.writemystory.pojos.User;

public interface DataManager {

    void registerObservers();

    void unregisterObservers();

    boolean isUserLoggedIn();

    boolean isUserAuthenticated();

    /**
     * @param user if null, the current user needs to be added.
     */
    void addNewUser(@Nullable User user);

    void requestStories();

    /**
     * @param storyId null, if you request a new, empty Story
     */
    void requestStory(@Nullable String storyId);

    /**
     * @param chapterId null, if you request a new, empty chapter
     * @param storyId
     * @param previousChapterId null, if this is the first chapter of a story
     * @param previousChoiceId null, if this is the first chapter of a story
     */
    void requestChapter(@Nullable String chapterId, @NonNull String storyId,
                        String previousChapterId, String previousChoiceId);

    /**
     * @param choiceId null, if you request a new, empty choice
     * @param storyId
     * @param previousChapterId
     */
    void requestChoice(@Nullable String choiceId, @NonNull String storyId,
                       @NonNull String previousChapterId);

    /**
     * @param textType
     * @param text          null or "" if you want to delete the draft
     * @param parentFieldId if you don't have one (eg. it's a new story or chapter)
     *                      you have to request a new id (requestNewId() method)
     */
    void saveDraft(EditTextType textType, @Nullable String text, @NonNull String parentFieldId);

    /**
     * @param textType
     * @param text
     * @param parentFieldId if you don't have one (eg. it's a new story or chapter)
     *                      you have to request a new id (requestNewId() method)
     */
    void saveText(EditTextType textType, @NonNull String text, @NonNull String parentFieldId);
}