package developer.arcfej.writemystory.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.example.myapp.MyEventBusIndex;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;
import java.util.Map;

import developer.arcfej.writemystory.model.authenticate.AuthenticateHelper;
import developer.arcfej.writemystory.model.authenticate.FirebaseAuthenticateHelper;
import developer.arcfej.writemystory.model.database.DatabaseContract;
import developer.arcfej.writemystory.model.database.DatabaseHelper;
import developer.arcfej.writemystory.model.database.FirestoreDatabaseHelper;
import developer.arcfej.writemystory.pojos.Chapter;
import developer.arcfej.writemystory.pojos.Choice;
import developer.arcfej.writemystory.pojos.Story;
import developer.arcfej.writemystory.pojos.User;
import developer.arcfej.writemystory.utilities.AnalyticsHelper;

import static developer.arcfej.writemystory.model.database.DatabaseContract.DbKey;

public class AppDataManager implements DataManager {

    private static AppDataManager instance = null;

    public static synchronized AppDataManager getInstance(FirebaseAnalytics firebaseAnalytics) {
        if (instance == null) {
            synchronized (AppDataManager.class) {
                instance = new AppDataManager(firebaseAnalytics);
            }
        }
        return instance;
    }

    private final FirebaseAnalytics firebaseAnalytics;
    private final EventBus modelBus;
    private final AuthenticateHelper authenticateHelper;
    private final DatabaseHelper databaseHelper;

    private AppDataManager(FirebaseAnalytics firebaseAnalytics) {
        this.firebaseAnalytics = firebaseAnalytics;
        modelBus = EventBus.builder().addIndex(new MyEventBusIndex()).build();
        authenticateHelper = FirebaseAuthenticateHelper.getInstance(firebaseAnalytics);
        databaseHelper = FirestoreDatabaseHelper.Companion.getInstance(
                new FirestoreDatabaseHelper.Parameters(firebaseAnalytics, modelBus));
    }

    @Override
    public void registerObservers() {
        if (!modelBus.isRegistered(this)) {
            modelBus.register(this);
        }
    }

    @Override
    public void unregisterObservers() {
        if (modelBus.isRegistered(this)) {
            modelBus.unregister(this);
        }
    }

    @Override
    public boolean isUserLoggedIn() {
        return authenticateHelper.isUserLoggedIn();
    }

    @Override
    public boolean isUserAuthenticated() {
        return authenticateHelper.isUserAuthenticated();
    }

    @Override
    public void addNewUser(@Nullable User user) {
        if (user == null) {
            user = authenticateHelper.getCurrentUser();
        }
        if (user.isAuthenticated) {
            databaseHelper.addNewUser(authenticateHelper.getCurrentUserId(), user);
            firebaseAnalytics.logEvent(AnalyticsHelper.Event.NEW_USER, null);
        }
    }

    @Override
    public void requestStories() {
        databaseHelper.queryPublishedStories();
    }

    @Override
    public void requestStory(@Nullable String storyId) {
        if (TextUtils.isEmpty(storyId)) {
            databaseHelper.addNewStory(
                    new Story(authenticateHelper.getCurrentUserId(), false));
        } else {
            databaseHelper.querySingleStory(storyId);
        }
    }

    @Override
    public void requestChapter(@Nullable String chapterId, @NonNull String storyId,
                               String previousChapterId, String previousChoiceId) {
        if (TextUtils.isEmpty(chapterId)) {
            databaseHelper.addNewChapter(
                    new Chapter(authenticateHelper.getCurrentUserId(), storyId,
                            previousChapterId, previousChoiceId, false)
            );
        } else {
            databaseHelper.querySingleChapter(chapterId);
        }
    }

    @Override
    public void requestChoice(@Nullable String choiceId, @NonNull String storyId,
                              @NonNull String previousChapterId) {
        if (TextUtils.isEmpty(choiceId)) {
            databaseHelper.addNewChoice(
                    new Choice(authenticateHelper.getCurrentUserId(), storyId,
                            previousChapterId, false)
            );
        } else {
            databaseHelper.querySingleChoice(choiceId);
        }
    }

    @Override
    public void saveDraft(EditTextType textType, String text, @NonNull String parentFieldId) {
        Map<DbKey, Object> updateValues;
        updateValues = new HashMap<>();
        switch (textType) {
            case TITLE:
                updateValues.put(DatabaseContract.DbKey.STORY_TITLE_DRAFT, text);
                databaseHelper.updateStory(parentFieldId, updateValues);
                break;
            case SHORT_DESCRIPTION:
                updateValues.put(DbKey.SHORT_DESCRIPTION_DRAFT, text);
                databaseHelper.updateStory(parentFieldId, updateValues);
                break;
            case CHAPTER_BODY:
                updateValues.put(DbKey.CHAPTER_BODY_DRAFT, text);
                databaseHelper.updateChapter(parentFieldId, updateValues);
                break;
            case DECISION_TEXT:
                // TODO necessary?
                break;
            case CHOICE:
                // TODO necessary?
                break;
        }
    }

    @Override
    public void saveText(EditTextType textType, @NonNull String text, @NonNull String parentFieldId) {
        Map<DbKey, Object> updateValues;
        updateValues = new HashMap<>();
        switch (textType) {
            case TITLE:
                updateValues.put(DatabaseContract.DbKey.STORY_TITLE, text);
                databaseHelper.updateStory(parentFieldId, updateValues);
                break;
            case SHORT_DESCRIPTION:
                updateValues.put(DbKey.SHORT_DESCRIPTION, text);
                databaseHelper.updateStory(parentFieldId, updateValues);
                break;
            case CHAPTER_BODY:
                updateValues.put(DbKey.CHAPTER_BODY, text);
                databaseHelper.updateChapter(parentFieldId, updateValues);
                break;
            case DECISION_TEXT:
                updateValues.put(DbKey.DECISION_TEXT, text);
                // TODO
                break;
            case CHOICE:
                updateValues.put(DatabaseContract.DbKey.CHOICE_TEXT, text);
                databaseHelper.updateChoice(parentFieldId, updateValues);
                break;
        }
    }

    @Subscribe
    public void onStoriesReady(StoriesReadyEvent event) {
        EventBus.getDefault().post(event);
    }

    @Subscribe
    public void onStoryReady(StoryReadyEvent event) {
        EventBus.getDefault().post(event);
    }

    public static class StoriesReadyEvent {

        public final Story[] stories;

        public StoriesReadyEvent(Story[] stories) {
            this.stories = stories;
        }
    }

    @Subscribe
    public void onChapterReady(ChapterReadyEvent event) {
        EventBus.getDefault().post(event);
    }

    @Subscribe
    public void onChoiceReady(ChoiceReadyEvent event) {
        EventBus.getDefault().post(event);
    }

    public static class StoryReadyEvent {

        public final String storyId;
        public final Story story;

        public StoryReadyEvent(String storyId, Story story) {
            this.storyId = storyId;
            this.story = story;
        }
    }

    public static class ChapterReadyEvent {

        public final String chapterId;
        public final Chapter chapter;

        public ChapterReadyEvent(String chapterId, Chapter chapter) {
            this.chapterId = chapterId;
            this.chapter = chapter;
        }
    }

    public static class ChoiceReadyEvent {

        public final String choiceId;
        public final Choice choice;

        public ChoiceReadyEvent(String choiceId, Choice choice) {
            this.choiceId = choiceId;
            this.choice = choice;
        }
    }
}