package developer.arcfej.writemystory;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import com.example.myapp.MyEventBusIndex;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.greenrobot.eventbus.EventBus;

import developer.arcfej.writemystory.model.AppDataManager;
import developer.arcfej.writemystory.model.DataManager;

public class WriteMyStory extends Application implements Application.ActivityLifecycleCallbacks {

    private DataManager dataManager;

    private int startedActivities;

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseAnalytics analytics = FirebaseAnalytics.getInstance(getApplicationContext());
        analytics.logEvent(FirebaseAnalytics.Event.APP_OPEN, null);

        // Use EventBus with indexes
        EventBus.builder().addIndex(new MyEventBusIndex()).installDefaultEventBus();
        dataManager = AppDataManager.getInstance(analytics);
        registerActivityLifecycleCallbacks(this);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    // Register the dataManager's observers, if there is only one activity started.
    // In this case the observers are unregistered
    @Override
    public void onActivityStarted(Activity activity) {
        if (startedActivities < 1) {
            dataManager.registerObservers();
        }
        startedActivities++;
    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    // Unregister the dataManager's observers, if this is the last activity to stopped
    @Override
    public void onActivityStopped(Activity activity) {
        startedActivities--;
        if (startedActivities < 1) {
            dataManager.unregisterObservers();
        }
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }
}