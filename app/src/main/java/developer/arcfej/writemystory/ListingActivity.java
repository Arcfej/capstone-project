package developer.arcfej.writemystory;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ViewGroup;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Arrays;
import java.util.List;

import developer.arcfej.writemystory.fragments.CardListFragment;
import developer.arcfej.writemystory.model.AppDataManager;
import developer.arcfej.writemystory.model.DataManager;
import developer.arcfej.writemystory.model.authenticate.FirebaseAuthenticateHelper;
import developer.arcfej.writemystory.pojos.Chapter;
import developer.arcfej.writemystory.pojos.ListType;
import developer.arcfej.writemystory.pojos.Story;
import developer.arcfej.writemystory.presenters.cardlist.ListContainerPresenter;
import developer.arcfej.writemystory.presenters.cardlist.ListContainerPresenterImpl;
import developer.arcfej.writemystory.utilities.AnalyticsHelper;
import developer.arcfej.writemystory.utilities.NetworkChangeReceiver;
import developer.arcfej.writemystory.views.BaseView;
import developer.arcfej.writemystory.views.cardlist.ListContainerViewImpl;

public class ListingActivity extends AppCompatActivity {

    private static final String LOG_TAG = ListingActivity.class.getSimpleName();

    private static final int REQUEST_SIGN_IN = 123;

    private static final String TAG_FRAGMENT_STORIES = "stories";
    private static final String TAG_FRAGMENT_CHAPTERS = "chapters";
    private static final String TAG_FRAGMENT_MY_STORIES = "my_stories";
    private static final String TAG_FRAGMENT_MY_CHAPTERS = "my_chapters";

    private static final String STATE_PRESENTER = "presenter_state";

    private FirebaseAnalytics firebaseAnalytics;
    private DataManager dataManager;
    private NetworkChangeReceiver networkChangeReceiver;

    // Dependency of this booleans: isOnline <- isLoggedIn <- hasView
    // isLoggedIn always false if not online.
    // hasView always false if not logged in.
    private boolean isOnline = false;
    private boolean isLoggedIn = false;
    private boolean hasView = false;

    // Saved state
    private Bundle presenterState;

    private AlertDialog offlineWarning;

    private BaseView view;
    private ListContainerPresenter presenter;
    private Fragment storiesFragment;
    private Fragment chaptersFragment;
    private Fragment myStoriesFragment;
    private Fragment myChaptersFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Restore state or initialize it
        if (savedInstanceState != null) {
            presenterState = savedInstanceState.containsKey(STATE_PRESENTER) ?
                    savedInstanceState.getBundle(STATE_PRESENTER) :
                    null;
        }
        // Initialize necessary fields
        firebaseAnalytics = FirebaseAnalytics.getInstance(getApplicationContext());
        dataManager = AppDataManager.getInstance(firebaseAnalytics);
        // A broadcast receiver to notify about network connection losses
        networkChangeReceiver = NetworkChangeReceiver.getInstance();
        presenter = new ListContainerPresenterImpl(getResources(), presenterState);
        isOnline = NetworkChangeReceiver.isConnected(this);
        // An AlertDialog which shown when network connection is lost
        offlineWarning = new AlertDialog.Builder(this)
                .setCancelable(false)
                .setMessage(R.string.warning_offline_message)
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        // Device is online, check if the user is logged in
                        if (!isLoggedIn) {
                            checkLogin();
                        }
                    }
                })
                .create();
        // Initialize fragments
        storiesFragment = getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT_STORIES);
        if (storiesFragment == null) {
            storiesFragment = new CardListFragment();
        }
        chaptersFragment = getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT_CHAPTERS);
        if (chaptersFragment == null) {
            chaptersFragment = new CardListFragment();
        }
        myStoriesFragment = getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT_MY_STORIES);
        if (myStoriesFragment == null) {
            myStoriesFragment = new CardListFragment();
        }
        myChaptersFragment = getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT_MY_CHAPTERS);
        if (myChaptersFragment == null) {
            myChaptersFragment = new CardListFragment();
        }
        // Login require network!
        if (isOnline) {
            checkLogin();
        } else {
            offlineWarning.show();
        }
    }

    /**
     * Call it only, if the device is online
     */
    private void checkLogin() {
        if (dataManager.isUserLoggedIn()) {
            isLoggedIn = true;
            setupUi();
        } else {
            logInWithFirebaseUi();
        }
    }

    /**
     * Called when the device is online and the user is logged in (guest/anonymous or not).
     * Called only in onCreate.
     */
    private void setupUi() {
        view = new ListContainerViewImpl(
                getLayoutInflater(),
                (ViewGroup) findViewById(android.R.id.content),
                presenter,
                dataManager.isUserAuthenticated()
        );
        setContentView(view.getRootView());
        hasView = true;
        presenter.attachView(view);
        // Customize the UI based on the displayed list
        Bundle upperArgs = new Bundle();
        upperArgs.putSerializable(CardListFragment.ARGUMENT_LIST_TYPE, presenter.getListType());
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (presenter.getListType()) {
            case STORIES:
                storiesFragment.setArguments(upperArgs);
                if (storiesFragment.isAdded()) {
                    transaction.show(storiesFragment);
                } else {
                    transaction.add(
                            R.id.fl_upper_fragment_holder,
                            storiesFragment,
                            TAG_FRAGMENT_STORIES
                    );
                }
                break;
            case CHAPTERS:
                chaptersFragment.setArguments(upperArgs);
                if (chaptersFragment.isAdded()) {
                    transaction.show(chaptersFragment);
                } else {
                    transaction.add(
                            R.id.fl_upper_fragment_holder,
                            chaptersFragment,
                            TAG_FRAGMENT_CHAPTERS
                    );
                }
                break;
            case MY_STORIES:
                myStoriesFragment.setArguments(upperArgs);
                if (myStoriesFragment.isAdded()) {
                    transaction.show(myStoriesFragment);
                } else {
                    transaction.add(
                            R.id.fl_upper_fragment_holder,
                            myStoriesFragment,
                            TAG_FRAGMENT_MY_STORIES
                    );
                }
            case MY_CHAPTERS:
                // Display lower CardListFragment only when MY_STORIES and _CHAPTERS are displayed
                Bundle lowerArgs = new Bundle();
                lowerArgs.putSerializable(CardListFragment.ARGUMENT_LIST_TYPE, ListType.MY_CHAPTERS);
                myChaptersFragment.setArguments(lowerArgs);
                if (myChaptersFragment.isAdded()) {
                    transaction.show(myChaptersFragment);
                } else {
                    transaction.add(
                            R.id.fl_lower_fragment_holder,
                            myChaptersFragment,
                            TAG_FRAGMENT_MY_CHAPTERS
                    );
                }
                break;
        }
        transaction.commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        EventBus.getDefault().register(presenter);
        registerReceiver(networkChangeReceiver, networkChangeReceiver.getIntentFilter());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle(STATE_PRESENTER, presenter.getState());
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(networkChangeReceiver);
        EventBus.getDefault().unregister(this);
        EventBus.getDefault().unregister(presenter);

        if (offlineWarning.isShowing()) {
            offlineWarning.dismiss();
        }
    }

    /**
     * Called when the device is online and nobody id logged in.
     */
    private void logInWithFirebaseUi() {
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build(),
                new AuthUI.IdpConfig.GoogleBuilder().build());
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        // TODO setLogo
                        .setTheme(R.style.AppTheme)
                        .build(),
                REQUEST_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Received after a login attempt
        if (requestCode == REQUEST_SIGN_IN) {
            onLoginResult(resultCode, data);
        }
    }

    private void onLoginResult(int resultCode, Intent data) {
        final IdpResponse response = IdpResponse.fromResultIntent(data);
        if (resultCode == RESULT_OK) {
            // Logged in user. Full functionality
            Bundle event = new Bundle();
            event.putString(FirebaseAnalytics.Param.METHOD, response.getProviderType());
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, event);
            if (response.isNewUser()) {
                dataManager.addNewUser(null);
            }
            isLoggedIn = true;
            restartActivity();
        } else if (response == null) {
            // Guest/anonymous user. Only reading allowed. Guest user counts as logged in user
            firebaseAnalytics.logEvent(AnalyticsHelper.Event.LOGIN_REFUSED, null);
            FirebaseAuth.getInstance().signInAnonymously()
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Bundle event = new Bundle();
                                event.putString(FirebaseAnalytics.Param.METHOD,
                                        task.getResult().getUser().getProviderId());
                                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, event);
                                isLoggedIn = true;
                                restartActivity();
                            }
                        }
                    });
        } else if (response.getError().getErrorCode() == ErrorCodes.NO_NETWORK) {
            // No internet connection
            isOnline = false;
            isLoggedIn = false;
            hasView = false;
        } else {
            // Unknown error
            Log.d(LOG_TAG, "Sign-in error: ", response.getError());
            isLoggedIn = false;
            hasView = false;
        }
    }

    private void restartActivity() {
        this.recreate();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        presenter.popListTypeStack();
        presenter.attachView(view);
    }

    @Subscribe
    public void onNetworkStateChanged(NetworkChangeReceiver.NetworkStateChangeEvent event) {
        if (event.isConnected) {
            isOnline = true;
            if (offlineWarning.isShowing()) {
                offlineWarning.dismiss();
            }
        } else {
            isOnline = false;
            if (!offlineWarning.isShowing()) {
                offlineWarning.show();
            }
        }
    }

    @Subscribe
    public void onLogout(FirebaseAuthenticateHelper.LogoutEvent event) {
        logInWithFirebaseUi();
    }

    @Subscribe
    public void onLowerHeaderClick(ListContainerViewImpl.LowerHeaderClickEvent event) {
        if (presenter.getListType() == ListType.STORIES) {
            Bundle upperArgs = new Bundle();
            upperArgs.putSerializable(CardListFragment.ARGUMENT_LIST_TYPE, ListType.MY_STORIES);
            myStoriesFragment.setArguments(upperArgs);
            Bundle lowerArgs = new Bundle();
            lowerArgs.putSerializable(CardListFragment.ARGUMENT_LIST_TYPE, ListType.MY_CHAPTERS);
            myChaptersFragment.setArguments(lowerArgs);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fl_upper_fragment_holder, myStoriesFragment, TAG_FRAGMENT_MY_STORIES)
                    .replace(R.id.fl_lower_fragment_holder, myChaptersFragment, TAG_FRAGMENT_MY_CHAPTERS)
                    .addToBackStack(TAG_FRAGMENT_MY_CHAPTERS)
                    .commit();
        }
    }

    @Subscribe
    public void onFabClick(ListContainerViewImpl.FabClickEvent event) {
        firebaseAnalytics.logEvent(AnalyticsHelper.Event.NEW_STORY_CREATED, null);
        Intent addNewStoryIntent = new Intent(this, EditingActivity.class);
        addNewStoryIntent.setAction(EditingActivity.ACTION_NEW_STORY);
        startActivity(addNewStoryIntent);
    }

    @Subscribe
    public void onStartReading(StartReadingEvent event) {
        Intent startReadingActivity = new Intent(this, ReadingActivity.class);
        startReadingActivity.putExtra(ReadingActivity.INTENT_EXTRA_STORY, event.story);
        if (event.chapter != null) {
            startReadingActivity.putExtra(ReadingActivity.INTENT_EXTRA_CHAPTER, event.chapter);
        }
        startActivity(startReadingActivity);
    }

    /**
     * The event must contain a story XOR a chapter
     */
    public static class StartReadingEvent {

        @Nullable
        public final Story story;
        @Nullable
        public final Chapter chapter;

        public StartReadingEvent(@Nullable Story story, @Nullable Chapter chapter) {
            this.story = story;
            this.chapter = chapter;
        }
    }
}