package developer.arcfej.writemystory;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import developer.arcfej.writemystory.fragments.ReadingFragment;
import developer.arcfej.writemystory.model.AppDataManager;
import developer.arcfej.writemystory.pojos.Chapter;
import developer.arcfej.writemystory.pojos.Story;
import developer.arcfej.writemystory.utilities.NetworkChangeReceiver;

public class ReadingActivity extends AppCompatActivity {

    private static final String LOG_TAG = ReadingActivity.class.getSimpleName();

    public static final String INTENT_EXTRA_STORY = "story";
    public static final String INTENT_EXTRA_CHAPTER = "chapter";

    private Story story;
    private Chapter chapter;

    private NetworkChangeReceiver networkChangeReceiver;
    private boolean isOnline = false;
    private AlertDialog offlineWarning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reading);

        evaluateIntent();
        if (savedInstanceState == null) {
            startNewFragment();
        }

        // A broadcast receiver to notify about network connection losses
        networkChangeReceiver = NetworkChangeReceiver.getInstance();
        isOnline = NetworkChangeReceiver.isConnected(this);
        // An AlertDialog which shown when network connection is lost
        offlineWarning = new AlertDialog.Builder(this)
                .setCancelable(false)
                .setMessage(R.string.warning_offline_message)
                .create();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (!isOnline) {
            offlineWarning.show();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        evaluateIntent();
        startNewFragment();
    }

    private void evaluateIntent() {
        Intent intent = getIntent();
        // Chapter xor story needed
        if (intent == null ||
                (!intent.hasExtra(INTENT_EXTRA_STORY) && !intent.hasExtra(INTENT_EXTRA_CHAPTER)) ||
                (intent.hasExtra(INTENT_EXTRA_STORY) && intent.hasExtra(INTENT_EXTRA_CHAPTER))
                ) {
            Log.e(LOG_TAG, "Reading activity needs a story xor chapter in it's intent");
            finish();
        }
        assert intent != null;
        story = intent.hasExtra(INTENT_EXTRA_STORY) ?
                (Story) intent.getParcelableExtra(INTENT_EXTRA_STORY) :
                null;
        chapter = intent.hasExtra(INTENT_EXTRA_CHAPTER) ?
                (Chapter) intent.getParcelableExtra(INTENT_EXTRA_CHAPTER) :
                null;
    }

    private void startNewFragment() {
        Fragment newFragment = new ReadingFragment();
        Bundle args = new Bundle();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (story != null) {
            setTitle(story.storyTitle);
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            args.putParcelable(ReadingFragment.ARGUMENT_STORY, story);
        } else if (chapter != null) {
            transaction.addToBackStack(null);
            args.putParcelable(ReadingFragment.ARGUMENT_CHAPTER, chapter);
        }
        newFragment.setArguments(args);
        transaction.replace(R.id.fl_fragment_holder, newFragment);
        transaction.commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        registerReceiver(networkChangeReceiver, networkChangeReceiver.getIntentFilter());
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        unregisterReceiver(networkChangeReceiver);

        if (offlineWarning.isShowing()) {
            offlineWarning.dismiss();
        }
    }

    @Subscribe
    public void onNetworkStateChanged(NetworkChangeReceiver.NetworkStateChangeEvent event) {
        if (event.isConnected) {
            isOnline = true;
            if (offlineWarning.isShowing()) {
                offlineWarning.dismiss();
            }
        } else {
            isOnline = false;
            if (!offlineWarning.isShowing()) {
                offlineWarning.show();
            }
        }
    }

    @Subscribe
    public void onChapterReady(AppDataManager.ChapterReadyEvent event) {
        Intent newIntent = new Intent(this, this.getClass());
        newIntent.putExtra(INTENT_EXTRA_CHAPTER, event.chapter);
        setIntent(newIntent);
        evaluateIntent();
        startNewFragment();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}